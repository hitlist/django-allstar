from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.db.models import Q
from django import forms
from .game import GameAdmin
from ..constants import GAME_TYPE_LIST
from ..models import Game, Season, Team, League, Conference, LeagueTeam, Stadium, TeamMember, Position, TeamRivalry

LEAGUES = League.objects.select_related()
SEASONS = Season.objects.select_related()
CONFERENCES = Conference.objects.select_related()
STADIUMS = Stadium.objects.select_related()
User = get_user_model()

def get_user_name(obj):
    return obj.user.get_full_name() or obj.user.username
get_user_name.short_description = 'Member'

class TeamForm(forms.ModelForm):
    model = LeagueTeam
    league = forms.ModelChoiceField(queryset = LEAGUES)
    season = forms.ModelChoiceField(queryset = SEASONS)
    conference = forms.ModelChoiceField(queryset = CONFERENCES)
    stadium = forms.ModelChoiceField(queryset = STADIUMS)

class LeagueTeamInline(admin.StackedInline):
    model = LeagueTeam
    form = TeamForm
    allow_add = False

class TeamSeasonFilter(admin.SimpleListFilter):
    title = 'season'
    parameter_name = 'season'
    def lookups(self, request, model_admin):
        return Season.objects.values_list()
    def queryset(self, request, queryset):
        value = self.value()
        if value is None:
            return queryset
        return queryset.filter(leagueteam__season=season)

class TeamAdmin(admin.ModelAdmin):
    inlines = [
        LeagueTeamInline
    ]

    list_display = ('__str__', 'year_established', 'wins', 'losses')
    list_filter = ('name', 'leagueteam__league__name', 'leagueteam__season')
    search_fields = ('name', 'city', )


class PositionAdmin(admin.ModelAdmin):
    model = Position
    ordering = ('category', 'title')
    list_display = ('title', 'category')

class TeamMemberAdmin(admin.ModelAdmin):
    model = TeamMember
    list_display = (get_user_name,'team', 'position',)
    list_filter = ('user', 'team__team', 'team__season', 'position',)
    list_select_related = ('user', 'team', 'position',)

class LeagueTeamAdmin(admin.ModelAdmin):
    list_filter = ('season', )
    list_select_related = ('season', 'league', 'team')

class LeagueTeamInline(admin.TabularInline):
    model = LeagueTeam
    allow_add = True

class LeagueForm(forms.ModelForm):
    model = League
    founders = forms.ModelMultipleChoiceField(
        queryset = User.objects.all(), required = True,
        widget=FilteredSelectMultiple(
            verbose_name='Founders',
            is_stacked=False
        )
    )

class LeagueAdmin(admin.ModelAdmin):
    form = LeagueForm
    inlines = [
        LeagueTeamInline
    ]
    def get_queryset(self, request):
        qs = super(LeagueAdmin, self).get_queryset(request)
        return qs.prefetch_related()
admin.site.register( Season )
admin.site.register( Position, PositionAdmin )
admin.site.register( Stadium )
admin.site.register( Team, TeamAdmin )
admin.site.register( League, LeagueAdmin)
admin.site.register( Conference )
admin.site.register( TeamMember, TeamMemberAdmin )
admin.site.register( LeagueTeam, LeagueTeamAdmin )
admin.site.register( Game, GameAdmin )
