=====
allstar
=====

Allstar is a Django based framework for build sports publishing websites

Quick start
-----------

* Quick summary
* Version
* .. _Learn reStructured Text: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html

How do I get set up?
~~~~~~~~~~~~~~~~~~~~

* Development setup instructions::
  $ mkdir cms-allstar
  $ cd cms-allstar/
  $ virtualenv -p python3.5 env
  $ source env/bin/activate
  $ pip install django==1.11.3
  $ django-admin startproject framework
  $ cd framework
  $ git init
  $ git remote add origin git@bitbucket.org:hitlist/django-allstar.git
  $ git fetch origin
  $ git merge origin/master
  $ pip install -r requirements.txt
  $ django-admin startproject framework

Contribution guidelines
~~~~~~~~~~~~~~~~~~~~~~~

* Writing tests
* Code review
* Other guidelines

Who do I talk to?
~~~~~~~~~~~~~~~~~

* Repo owner or admin
* Other community or team contact
