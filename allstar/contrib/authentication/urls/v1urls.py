from allstar.api import AllStarApi
from django.conf.urls import patterns, url, include
from ..api.v1.resources import ( SessionResource, GroupResource, UserResource, PermissionResource)

authentication_v1_urls = AllStarApi(api_name="v1/auth")

authentication_v1_urls.register( SessionResource() )
authentication_v1_urls.register( GroupResource() )
authentication_v1_urls.register( UserResource() )
authentication_v1_urls.register( PermissionResource() )


urlpatterns = patterns("", url('', include( authentication_v1_urls.urls ) ) )
