# Create your views here.
import datetime
from socket import gethostname

from allstar.contrib.authentication.forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Permission, User
from django.core.cache import cache
from django.template.loader import render_to_string
from django.db import transaction
try:
    import cPickle as pickle
except:
    import pickle



def user_login(request,  bundle ):
    f = LoginForm( bundle.data )
    if f.is_valid():
        user = authenticate(username=f.cleaned_data["username"], password=f.cleaned_data["password"] )

        if( user and user.is_active ):
            login(request, user )
            return user
    else:
        return None

def user_logout( request ):
    user = request.user
    if user.is_authenticated():
        key = ( "USER:%s:PERMISSIONS" % request.user.pk )
        cache.delete( key )

    #send logout message

    logout( request )
    return True

def switch_user(request, pk=None):
    if request.user.is_staff and request.user.has_perm("auth.change_user"):
        if pk:
            user = User.objects.get(pk=pk)
            if user.is_active:
                # This is done by the django authenticate metion even though
                # the docs imply that authenicate is optional

                #since we are not callin that method, steal it from a known user
                try:
                    login(request, user )
                except:
                    pass
                return user
            else:
                # switch back and clear values
                pass
    else:
        return None

def perms_for_user( user, appname=None ):
    permissions = {}
    # fast pass for anonymous users
    if user.is_anonymous():
        return permissions

    # generate the cache key
    if appname is None:
        CACHE_KEY = ( "USER:%s:PERMISSIONS" % user.pk )
    else:
        CACHE_KEY = ( "USER:%s:PERMISSIONS:%s" % (user.pk, appname.upper() ) )

    # fast pass for cached perms
    cached = cache.get( CACHE_KEY )
    if cached:
        return pickle.loads(cached)

    # if we got this far, we need to do some work!

    # get all perms for user
    if( user.is_superuser ):
        user_perms = Permission.objects.select_related()
    else:
        user_perms = set( Permission.objects.select_related().filter(group__user=user))
        user_perms.update(user.user_permissions.select_related())

    # create list of  [ app label, content type, perm code ]s  separated permission string
    # where app_lable matches appname if appname is not none
    perms  = [
        [
            unicode(p.content_type.app_label)
            , unicode(p.content_type)
            , unicode(p.codename)
        ]
        for p in user_perms
            if p.content_type.app_label.replace(" ", "_") == appname or appname is None
    ]

    for _p in perms:
        current_app = appname or _p[0]
        current_type = _p[1].lower().replace(" ", "_")
        current_perm = _p[2]

        if permissions.has_key( current_app ) is False:

            permissions[ current_app ] = {current_type:{ current_perm:1} }
        else:
            if permissions[current_app].has_key( current_type ) is False:
                 permissions[current_app][current_type] = {current_perm:1}
            else:
                permissions[ current_app ][ current_type ][current_perm] = 1

    if appname is None:
        cache.set( CACHE_KEY, pickle.dumps(permissions ) )
        return permissions
    else:
        cache.set(CACHE_KEY , pickle.dumps( permissions[ appname ] ) )
        return permissions[ appname ]

def reset_password( user_pk, pw1, pw2 ):
    if pw1 is None and pw2 is None:
        return False 


    is_valid = pw1.strip() == pw2.strip()

    if is_valid:
        user = User.objects.get( pk=user_pk )
        user.set_password( pw2 )
        user.save()
        return True
        
    return False
