from django import forms
from django.contrib import admin
from django.db.models import Q
from ..constants import GAME_TYPE_LIST
from ..models import (Game, LeagueTeam, TeamMember, PassingStatistic,
                      RushingStatistic, ReceivingStatistic, BlockingStatistic,
                      DefensiveStatistic, KickingStatistic,
                      KickReturningStatistic, PuntingStatistic,
                      PuntReturningStatistic, Team)

TEAMS = LeagueTeam.objects.select_related()
PLAYERS = TeamMember.objects.none()


def get_game_type(obj):
    game_type = obj.game_type
    return GAME_TYPE_LIST[game_type - 1]


def get_game_status(obj):
    return obj.get_status_display()


get_game_type.short_description = 'Game Type'
get_game_status.short_description = 'Game Status'


class PlayerForm(forms.ModelForm):
    player = forms.ModelChoiceField(queryset=PLAYERS)


class PassingStatisticsInline(admin.TabularInline):
    model = PassingStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class RushingStatisticsInline(admin.TabularInline):
    model = RushingStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class ReceivingStatisticsInline(admin.TabularInline):
    model = ReceivingStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class BlockingStatisticsInline(admin.TabularInline):
    model = BlockingStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class DefensiveStatisticsInline(admin.TabularInline):
    model = DefensiveStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class KickingStatisticsInline(admin.TabularInline):
    model = KickingStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class KickReturningStatisticsInline(admin.TabularInline):
    model = KickReturningStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class PuntingStatisticsInline(admin.TabularInline):
    model = PuntingStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class PuntReturningStatisticsInline(admin.TabularInline):
    model = PuntReturningStatistic
    classes = ('grp-collapse grp-closed', )
    allow_add = True
    raw_id_fields = ('player', )
    autocomplete_lookup_fields = {
        'fk': ['player'],
    }


class GameForm(forms.ModelForm):
    model = Game
    away_team = forms.ModelChoiceField(queryset=TEAMS)
    home_team = forms.ModelChoiceField(queryset=TEAMS)


class GameTeamFilter(admin.SimpleListFilter):
    title = 'team'
    parameter_name = 'team'

    def lookups(self, request, model_admin):
        return Team.objects.values_list('id', 'name')

    def queryset(self, request, queryset):
        value = self.value()
        if value is None:
            return queryset

        return queryset.filter(
            Q(home_team__team=value) | Q(away_team__team=value)
        )


class GameAdmin(admin.ModelAdmin):
    model = Game
    form = GameForm
    list_per_page = 10
    date_hierarchy = 'date_played'
    inlines = [
        PassingStatisticsInline, RushingStatisticsInline,
        ReceivingStatisticsInline, BlockingStatisticsInline,
        DefensiveStatisticsInline, KickingStatisticsInline,
        KickReturningStatisticsInline, PuntingStatisticsInline,
        PuntReturningStatisticsInline
    ]
    list_display = (
        'home_team',
        'home_score',
        'away_team',
        'away_score',
        get_game_type,
        get_game_status,
    )
    list_filter = ('home_team__season', 'home_team__league', GameTeamFilter)

    def get_queryset(self, request):
        qs = super(GameAdmin, self).get_queryset(request)
        return qs.select_related('home_team', 'away_team', 'home_team__team',
                                 'away_team__team', 'home_team__league',
                                 'home_team__season', 'away_team__league',
                                 'away_team__season')
