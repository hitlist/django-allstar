from django.conf.urls import include, url
from django.core.validators import slug_re
from django.template.defaultfilters import slugify

res = {
    'ct': r'(?P<content_type>[a-z][a-z0-9-]+)',
    'year': r'(?P<year>\d{4})',
    'month': r'(?P<month>\d{1,2})',
    'day': r'(?P<day>\d{1,2})',
    'rest': r'(?P<url_remainder>.+/)',
    'slug': r'(?P<slug>%s)' % slug_re.pattern.strip('^$'),
    'pk': r'(?P<pk>[0-9a-f-]+)',
    'ct_id': r'(?P<ct_id>\d+)',
}
