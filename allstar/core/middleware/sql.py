import logging
import sqlparse
from django.utils.deprecation import MiddlewareMixin
from django.db import connection
from django.conf import settings
from django.utils.termcolors import colorize, PALETTES, make_style

YELLOW = make_style(opts=("bold",) , fg="yellow")
BLUE = make_style(opts=("bold",) , fg="blue")
RED_ON_BLACK = make_style(opts=('bold',), fg="red", bg="black")

DEBUG = getattr(settings, 'DEBUG', False)
DEBUG_SQL_TRACE = getattr(settings, 'DEBUG_SQL_TRACE', False)
DEBUG_PARSE_SQL = getattr(settings, 'DEBUG_PARSE_SQL', False)

KEYWORDS = {
    "AND":make_style(opts=("bold", "underscore"), fg="cyan" ),
    "DELETE":make_style(opts=("blink","bold"), fg="cyan", bg='black'),
    "DISTINCT": YELLOW,
    "FROM": YELLOW,
    "LEFT": make_style(opts=("bold",), fg="blue", bg="white"),
    "INNER":RED_ON_BLACK,
    "INSERT":make_style(opts=("bold",), fg="magenta" ),
    "JOIN":RED_ON_BLACK,
    "NOT":make_style(opts=('bold',), fg="red"),
    "ON":make_style(opts=("bold","underscore"), fg="magenta"),
    "ORDER BY":BLUE,
    "OUTER":make_style( opts=('bold',) ,fg="cyan", bg="black"),
    "SELECT":make_style(opts=('bold',) , fg="white", bg="blue"),
    "SET": make_style(fg="green") ,
    "text":make_style( fg="white"),
    "UPDATE":make_style( opts = ('bold', ), fg = "blue" ),
    "VALUES": make_style( opts=("bold", ),fg="yellow" ),
    "WHERE":make_style( opts=("bold",), fg="green" ),
    "COUNT": make_style(fg="magenta", opts=('bold',)),
    "COALESCE": make_style(fg="blue"),
    "MAX": make_style(fg="blue"),
    "MIN": make_style(fg="blue"),
    "SUM": make_style(fg="blue"),
    "ARRAY": make_style(fg="magenta"),
}

class TerminalLoggingMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        if DEBUG and DEBUG_SQL_TRACE:
            for query in connection.queries :

                _sql = query["sql"]
                execution_time = query["time"]
                execution_info = ' \033[1;31m -- [Execution time: %s]\033[0m' % execution_time

                if DEBUG_PARSE_SQL:
                    _sql = "{0}\n{1}".format( sqlparse.format(_sql, reindent=True), execution_info )
                else:
                    _sql = _sql + execution_info

                for k,v in KEYWORDS.items():
                    if k not in ['time', 'text']:
                        _sql = _sql.replace( k, v(k) )

                print (_sql)


        return response
