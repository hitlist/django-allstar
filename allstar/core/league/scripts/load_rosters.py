#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been automatically generated.
# Instead of changing it, create a file called import_helper.py
# and put there a class called ImportHelper(object) in it.
#
# This class will be specially casted so that instead of extending object,
# it will actually extend the class BasicImportHelper()
#
# That means you just have to overload the methods you want to
# change, leaving the other ones inteact.
#
# Something that you might want to do is use transactions, for example.
#
# Also, don't forget to add the necessary Django imports.
#
# This file was generated with the following command:
# ./manage.py dumpscript league.TeamMember
#
# to restore it, run
# manage.py runscript module_name.this_script_name
#
# example: if manage.py is at ./manage.py
# and the script is at ./some_folder/some_script.py
# you must make sure ./some_folder/__init__.py exists
# and run  ./manage.py runscript some_folder.some_script
import os, sys
import random
from random import choice, randint
from datetime import datetime
from django.db import transaction
from decimal import Decimal
from django.contrib.contenttypes.models import ContentType
from ..constants import OFFENSIVE_POSITIONS, DEFENSIVE_POSITIONS, SPECIAL_POSITIONS

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
names = [name.replace('\n', '') for name in open(os.path.join(BASE_DIR, 'words.txt'))]

class BasicImportHelper(object):

    def pre_import(self):
        pass

    @transaction.atomic
    def run_import(self, import_data):
        import_data()

    def post_import(self):
        pass

    def locate_similar(self, current_object, search_data):
        # You will probably want to call this method from save_or_locate()
        # Example:
        #   new_obj = self.locate_similar(the_obj, {"national_id": the_obj.national_id } )

        the_obj = current_object.__class__.objects.get(**search_data)
        return the_obj

    def locate_object(self, original_class, original_pk_name, the_class, pk_name, pk_value, obj_content):
        # You may change this function to do specific lookup for specific objects
        #
        # original_class class of the django orm's object that needs to be located
        # original_pk_name the primary key of original_class
        # the_class      parent class of original_class which contains obj_content
        # pk_name        the primary key of original_class
        # pk_value       value of the primary_key
        # obj_content    content of the object which was not exported.
        #
        # You should use obj_content to locate the object on the target db
        #
        # An example where original_class and the_class are different is
        # when original_class is Farmer and the_class is Person. The table
        # may refer to a Farmer but you will actually need to locate Person
        # in order to instantiate that Farmer
        #
        # Example:
        #   if the_class == SurveyResultFormat or the_class == SurveyType or the_class == SurveyState:
        #       pk_name="name"
        #       pk_value=obj_content[pk_name]
        #   if the_class == StaffGroup:
        #       pk_value=8

        search_data = { pk_name: pk_value }
        the_obj = the_class.objects.get(**search_data)
        #print(the_obj)
        return the_obj


    def save_or_locate(self, the_obj):
        # Change this if you want to locate the object in the database
        try:
            the_obj.save()
        except:
            print("---------------")
            print("Error saving the following object:")
            print(the_obj.__class__)
            print(" ")
            print(the_obj.__dict__)
            print(" ")
            print(the_obj)
            print(" ")
            print("---------------")

            raise
        return the_obj


importer = None
try:
    import import_helper
    # We need this so ImportHelper can extend BasicImportHelper, although import_helper.py
    # has no knowlodge of this class
    importer = type("DynamicImportHelper", (import_helper.ImportHelper, BasicImportHelper ) , {} )()
except ImportError as e:
    # From Python 3.3 we can check e.name - string match is for backward compatibility.
    if 'import_helper' in str(e):
        importer = BasicImportHelper()
    else:
        raise


try:
    import dateutil.parser
except ImportError:
    print("Please install python-dateutil")
    sys.exit(os.EX_USAGE)

def run():
    importer.pre_import()
    importer.run_import(import_data)
    importer.post_import()

def import_data():
    # Initial Imports
    from uuid import uuid4
    from allstar.core.league.models.team import Position
    from allstar.contrib.authentication.models import Allstar
    from allstar.core.league.models.league import Season, LeagueTeam
    from allstar.core.league.models.team import Team, TeamMember

    # Processing model: allstar.core.league.models.team.TeamMember
    players = {}
    positions = Position.objects.filter(abbr__in=OFFENSIVE_POSITIONS + DEFENSIVE_POSITIONS + SPECIAL_POSITIONS)
    def genplayers():
        random.seed(randint(1, 1000))
        random.shuffle(names)
        def getname():
            fname = choice(names)
            lname = choice(names)
            username = '{0}_{1}_{2}'.format(fname, lname, str(uuid4())[0:5])
            return {'first_name': fname, 'last_name': lname, 'username': username, 'email': '{0}@mail.com'.format(username)}

        users = Allstar.objects.bulk_create([
            Allstar(
                password = 'pbkdf2_sha256$36000$mhwZmaTyN58b$pal/idUZN+xvyG5gSSLJsIbQrp4H8yVV1gYG1A6j//s=',
                is_superuser = False,
                is_staff = True,
                is_active = True,
                date_joined = datetime.now(),
                archived = None,
                avatar = None,
                biography = '',
                standing = 1,
                birthday = None,
                hs_attended = None,
                college_attened = None,
                height_feet = 6,
                height_inches = choice([1,2,3,4,5,6,7]),
                gallery = None,
                **getname()
            ) for x in range(50)
        ])
        return users

    for team in LeagueTeam.objects.all().distinct('team', 'season').order_by('season__pk'):
        numbers = [x for x in range(1, 100)]
        def getnumber():
            num = choice(numbers)
            numbers.remove(num)
            return num

        TeamMember.objects.bulk_create([
            TeamMember(
                user = player,
                number = getnumber(),
                position = choice(positions),
                team = team
            ) for player in genplayers()
        ])

