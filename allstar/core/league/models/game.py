import datetime
from uuid import uuid4
from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from allstar.core.models import TimeStampedModel, ModelBase
from ..constants import ( GAME_TYPES, GAME_TYPE_CHAMPIONSHIP, GAME_TYPE_PLAYOFF, GAME_TYPE_REGULARSEASON, GAME_TYPE_PRESEASON, STADIUM_FIELD_TYPES, GAME_STATUS)


User = get_user_model()

class Game( TimeStampedModel ):
    '''
    The game is the way our system monitors all games played for all teams in the league - including the
    default team. This is how we are able to calculate wins, losses, ties, etc.
    '''
    id          = models.UUIDField(db_column="game_id", primary_key=True, db_index=True, default=uuid4, editable=False )
    game_type   = models.IntegerField( choices=GAME_TYPES, default=2)
    date_played = models.DateTimeField(db_index=True)
    status      = models.SmallIntegerField(_('Status'), choices=GAME_STATUS, default=1, db_index=True)
    away_team   = models.ForeignKey('LeagueTeam', related_name='awayteam', on_delete=models.CASCADE)
    away_score  = models.IntegerField(default=0, blank=False)

    home_team   = models.ForeignKey('LeagueTeam', related_name='hometeam', on_delete=models.CASCADE)
    home_score  = models.IntegerField(default=0, blank=False)

    class Meta:
        ordering            = ['-date_played']
        app_label           = "league"
        db_table            = "allstar_league_game"
        verbose_name        = _(u"Game")
        verbose_name_plural = _(u"Games")
        get_latest_by       = "-date_played"
        index_together = (
            ('status', ),
            ('status', 'home_score', 'away_score'),
        )

    def __str__(self):
        return '%s at %s' % (self.away_team, self.home_team)

    def __unicode__(self):
        return unicode(sel.__self__())

    @property
    def winner( self ):
        return self.get_winner()

    def set_score(self, new_home_score=None, new_away_score=None):
        if new_home_score is not None:
            self.home_score = new_home_score
        if new_away_score is not None:
            self.away_score = new_away_score
        return self.save()

    def get_winner(self):
        if self.home_score > self.away_score:
            return self.home_team
        elif self.home_score < self.away_score:
            return self.away_team
        elif self.home_score == self.away_score:
            return 'Tie'
        else: return ''


class Statistic(models.Model):
    player = models.ForeignKey('TeamMember', related_name='%(app_label)s_%(class)s_related', related_query_name='%(app_label)s_%(class)ss', verbose_name = _('player'), on_delete=models.CASCADE)
    game = models.ForeignKey('Game', related_name='%(app_label)s_%(class)s_related', related_query_name='%(app_label)s_%(class)ss', verbose_name = _('game'), on_delete=models.CASCADE)

    class Meta:
        abstract = True

class PassingStatistic(Statistic):
    id            = models.UUIDField(db_column='passing_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    attempts      = models.PositiveSmallIntegerField(_('Passing Attempts'), db_column='passing_attempts', default = 0)
    completions   = models.PositiveSmallIntegerField(_('Passing Completions'), db_column='passing_completions', default = 0)
    yards         = models.DecimalField(_('Passing Yards'), db_column='passing_yards', decimal_places = 1, default = 0, max_digits = 5)
    touchdowns    = models.PositiveSmallIntegerField(_('Passing Touchdowns'), db_column='passing_touchdowns', default = 0)
    interceptions = models.PositiveSmallIntegerField(_('Passing Interceptions'), db_column='passing_interceptions', default = 0)

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_passing_statistic'
        verbose_name        = 'Passing Statistics'
        verbose_name_plural = 'Passing Statistics'

class RushingStatistic(Statistic):
    id         = models.UUIDField(db_column='rushing_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    carries    = models.SmallIntegerField(_('Carries'), db_column='rushing_carries', default = 0)
    yards      = models.DecimalField(_('Total Yards'), db_column='rusing_total_yards', decimal_places = 1, default = 0, max_digits = 5)
    yac        = models.DecimalField(_('Yds After Contact'),db_column='rushing_yards_after_contact', decimal_places = 1, default = 0, max_digits = 5)
    touchdowns = models.SmallIntegerField(_('Touchdowns'), db_column='rushing_touchdowns', default = 0)
    fumbles    = models.PositiveSmallIntegerField(_('Fumbles'), db_column='rushing_rushing_fubles', default = 0)

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_rushing_statistic'
        verbose_name        = 'Rushing Statistics'
        verbose_name_plural = 'Rushing Statistics'

class ReceivingStatistic(Statistic):
    id         = models.UUIDField(db_column='receiving_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    catches    = models.PositiveSmallIntegerField(_('Catches'), db_column = 'receiving_catche', default = 0)
    drops      = models.PositiveSmallIntegerField(_('Drops'), db_column = 'receiving_drop', default = 0)
    yards      = models.DecimalField(_('Total Yards'), decimal_places = 1, db_column = 'receiving_total_yard', default = 0, max_digits = 5)
    yac        = models.DecimalField(_('Yds After Catch'), db_column='receiving_yards_after_catch', decimal_places = 1, default = 0, max_digits = 5)
    touchdowns = models.SmallIntegerField(_('Touchdowns'), db_column = 'receiving_touchdown', default = 0)

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_receiving_statistic'
        verbose_name        = 'Receiving Statistics'
        verbose_name_plural = 'Receiving Statistics'

class BlockingStatistic(Statistic):
    id            = models.UUIDField(db_column='blocking_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    sacks_allowed = models.PositiveSmallIntegerField(_('Sacks Allowed'), db_column = 'blocking_sacks_allowed', default = 0)
    pancakes      = models.PositiveSmallIntegerField(_('Pancake Blocks'), db_column = 'blocking_pancake', default = 0)
    key_blocks    = models.PositiveSmallIntegerField(_('Key Block'), db_column = 'blocking_key_block', default = 0)
    missed_blocks = models.PositiveSmallIntegerField(_('Missed Blocks'), db_column = 'blocking_missed_block', default = 0)

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_blocking_statistic'
        verbose_name        = 'Blocking Statistics'
        verbose_name_plural = 'Blocking Statistics'

class DefensiveStatistic(Statistic):
    id              = models.UUIDField(db_column='defensive_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    tackle          = models.PositiveSmallIntegerField(_('Tackles'), db_column = 'defensive_tackle', default = 0)
    forced_fumble   = models.PositiveSmallIntegerField(_('Forced Fumbles'), db_column = 'defensive_forced_fumble', default = 0)
    fumble_recovery = models.PositiveSmallIntegerField(_('Fumble Recoveries'), db_column = 'defensive_fumble_recovery', default = 0)
    touchdown       = models.PositiveSmallIntegerField(_('Touchdowns'), db_column = 'defensive_touchdown', default = 0)
    sack            = models.PositiveSmallIntegerField(_('Sacks'), db_column = 'defensive_sack', default = 0)
    interception    = models.PositiveSmallIntegerField(_('Interceptions'), db_column = 'defensive_interception', default = 0)

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_defensive_statistic'
        verbose_name        = 'Defensive Statistics'
        verbose_name_plural = 'Defensive Statistics'

class KickingStatistic(Statistic):
    id                   = models.UUIDField(db_column='kicking_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    field_goal_attampts  = models.PositiveSmallIntegerField(_('FG Attempts'), db_column = 'kicking_field_goal_attempt', default = 0)
    field_goal_made      = models.PositiveSmallIntegerField(_('FG Made'), db_column = 'kicking_field_goal_made', default = 0)
    field_goal_yards     = models.PositiveIntegerField(_('FG Yards'), db_column = 'kicking_field_goal_yards', default = 0)
    field_goal_longest   = models.PositiveSmallIntegerField(_('Longest FG'), db_column = 'kicking_field_goal_longest', default = 0)
    extra_point_attempts = models.PositiveSmallIntegerField(_('XP Attempts'), db_column = 'kicking_extra_point_attempt', default = 0)
    extra_point_made     = models.PositiveSmallIntegerField(_('XP Made'), db_column = 'kicking_extra_point_made', default = 0)

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_kicking_statistic'
        verbose_name        = 'Kicking  Statistics'
        verbose_name_plural = 'Kicking Statistics'

class PuntingStatistic(Statistic):
    id            = models.UUIDField(db_column='punting_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    punts         = models.PositiveSmallIntegerField(_('Total Punts'), db_column = 'puting_total_punts', default = 0)
    total_yards   = models.DecimalField(_('Total Yards'), db_column = 'punting_total_yards', decimal_places = 1, default = 0, max_digits = 5)
    net_yards     = models.DecimalField(_('Net Yards'), db_column = 'puting_net_yards', decimal_places = 1, default = 0, max_digits = 5)
    touchback     = models.PositiveSmallIntegerField(_('Touchbacks'), db_column = 'punting_touchback', default = 0)
    inside_twenty = models.PositiveSmallIntegerField(_('Inside 20'), db_column = 'punting_inside_twenty', default = 0)
    longest       = models.DecimalField(_('Longest Punt'), db_column = 'punting_longest_punt', decimal_places = 1, default = 0, max_digits = 5)

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_punting_statistic'
        verbose_name        = 'Puntinging  Statistics'
        verbose_name_plural = 'Punting Statistics'

class KickReturningStatistic(Statistic):
    id          = models.UUIDField(db_column='kick_return_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    returns     = models.PositiveSmallIntegerField(_('KO Returns'), db_column = 'kick_return_return', default = 0)
    total_yards = models.DecimalField(_('KO Return Yards'), db_column = 'kick_return_total_yards', decimal_places = 1, default = 0, max_digits = 5)
    touchdowns  = models.PositiveSmallIntegerField(_('KO Return Touchdowns'), db_column = 'kick_return_touchdown', default = 0)
    fumbles     = models.PositiveSmallIntegerField(_('KO Return Fumbles'), db_column = 'kick_return_fumble', default = 0)
    muffs       = models.PositiveSmallIntegerField(_('KO Ret. Muffs'), db_column = 'kick_return_muff', default = 0)
    longest     = models.SmallIntegerField(_('Longest Return'), db_column = 'kick_return_longest_return', default = 0)

    class Meta:
        app_label = 'league'
        db_table = 'allstar_league_kick_return_statistic'
        verbose_name = 'Kick Returning  Statistics'
        verbose_name_plural = 'Kick Returning Statistics'

class PuntReturningStatistic(Statistic):
    id         = models.UUIDField(db_column='kick_return_statistic_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    returns    = models.PositiveSmallIntegerField(_('Punt Returns'), db_column = 'punt_return_return', default = 0)
    yards      = models.DecimalField(_('Total Punt Ret. Yards'), db_column = 'punt_return_yard', decimal_places = 1, default = 0, max_digits = 5)
    touchdowns = models.PositiveSmallIntegerField(_('Punt Ret. TD'), db_column = 'punt_return_touchdown', default = 0)
    fumbles    = models.PositiveSmallIntegerField(_('Fumbles'), db_column = 'punt_return_fumbles', default = 0)
    muffs       = models.PositiveSmallIntegerField(_('Muffs'), db_column = 'punt_return_muff', default = 0)

    class Meta:
        app_label = 'league'
        db_table = 'allstar_league_punt_return_statistic'
        verbose_name = 'Punt Returning  Statistics'
        verbose_name_plural = 'Punt Returning Statistics'
