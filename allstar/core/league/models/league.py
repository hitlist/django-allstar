import datetime
from uuid import uuid4
from django.db import models
from django.db.models import Q, F, Sum, Value, Case, When, Count, Subquery
from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.utils.text import slugify
from django.utils.functional import cached_property
from django.contrib.auth import get_user_model
from allstar.core.models import TimeStampedModel, ModelBase
from allstar.core.managers import ArchiveManager
from django.utils.translation import ugettext_lazy as _
from .game import Game
from .team import Team

def now():
    return datetime.date.today().year

class League(TimeStampedModel):
    id               = models.UUIDField( db_column="league_id", primary_key=True, db_index=True, default=uuid4, editable=False )
    logo             = models.ForeignKey('darkroom.Photo', null=True, blank=True, default=None, on_delete=models.CASCADE)
    name             = models.CharField( verbose_name=_(u"League Name"), db_column="league_name", max_length=75, db_index=True )
    abbreviation     = models.CharField( verbose_name=_(u"League Abbreviation"), db_column="league_abbr",max_length=6, db_index=True )
    decommision_date = models.DateField( verbose_name=_(u"Decommissioned"), blank=True, null=True )
    founded          = models.DateField( verbose_name=_(u"Date Founded"), blank=False, null=True )
    founders         = models.ManyToManyField( settings.AUTH_USER_MODEL, null=True, blank=True )
    admin_objects    = models.Manager()
    objects          = ArchiveManager()

    class Meta:
        app_label = 'league'
        db_table = 'allstar_league'

    def __str__(self):
        return self.abbreviation
    def __unicode__(self):
        return unicode(self.abbreviation)

    def get_absolute_url(self):
        return reverse_lazy('allstar-league-detail', kwargs = {
            'ct_id':self.get_ct_id(),
            'content_type': self.get_ct().name,
            'slug': self.abbreviation.lower(),
            'pk': str(self.pk)
        })

    def get_league_stats(self, season = None):
        '''
        returns a dictionary with the total number of
        * teams
        * games
        * points

        for a given season, or all time if not specified
        '''
        hometeam = Q(home_team__league=self.pk)
        awayteam = Q(away_team__league=self.pk)
        games = Game.objects.filter(hometeam|awayteam).distinct()
        teams = self.teams(season = season)
        out = games.aggregate(
          points = (Sum('home_score') + Sum('away_score'))
        , games = Count('pk')
        )
        out['teams'] = teams.count()
        return out

    def teams(self, season = None):
        home_team_league = Q(leagueteam__league=self)
        return Team.objects.filter(home_team_league).distinct('pk')

    def add_team(self, team, season, conference, stadium):
        return LeagueTeam(
          team = team
        , season = season
        , conference = conference
        , stadium = stadium
        ).save()

class Conference(TimeStampedModel):
    """
    Represents a logical separation of teams within a league
    """
    id            = models.UUIDField(db_column="conference_id", primary_key=True, db_index=True, default=uuid4, editable=False )
    name          = models.CharField( max_length=75, verbose_name=_("Name"), db_index=True, db_column="conference_name" )
    logo          = models.ForeignKey( 'darkroom.Photo', verbose_name=_(u'Logo'), db_column="conference_logo_id", on_delete=models.CASCADE, null=True)

    slug          = models.SlugField( blank=True, null=False, editable=False )

    admin_objects = models.Manager()
    objects       = ArchiveManager()

    class Meta:
        app_label = "league"
        db_table = "allstar_league_conference"
        abstract = False

    def __str__(self):
        return '{} Confernce'.format(self.name)

    def __unicode__( self ):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(Conference, self).save(*args, **kwargs)


class LeagueTeam( ModelBase ):
    """

    """
    id            = models.UUIDField(db_column="league_team_id", primary_key=True, db_index=True, default=uuid4, editable=False )
    team          = models.ForeignKey( "Team", verbose_name=_(u'Team'), on_delete=models.CASCADE)
    league        = models.ForeignKey( "League", verbose_name=_(u'League'), on_delete=models.CASCADE)
    season        = models.ForeignKey( "Season", verbose_name=_(u'Season'), on_delete=models.CASCADE)
    conference    = models.ForeignKey( "Conference", verbose_name=_(u'Conference'), on_delete=models.CASCADE)
    stadium       = models.ForeignKey( "Stadium", verbose_name=_(u'Stadium'), on_delete=models.CASCADE)

    admin_objects = models.Manager()
    objects       = ArchiveManager()

    class Meta:
        app_label           = "league"
        db_table            = "allstar_league_team_membership"
        unique_together     =( ( "season", "league", "team" ), )
        verbose_name        = "League Member"
        verbose_name_plural = "League Members"

    def __str__( self ):
        return self.display_name

    @cached_property
    def display_name(self):
        return "( %s ) %s %s" %( self.season.year, self.league, self.team.full_name )

    @cached_property
    def team_name(self):
        return self.team.name
    def __unicode__(self):
        return unicode(self.__str__())

class Season( TimeStampedModel ):
    '''
    A Season is a container for all games & teams in any given year
    '''
    year          = models.PositiveSmallIntegerField(verbose_name=_("Season Year"), db_index=True, primary_key=True, db_column=("season_year"), unique=True, default=now)

    admin_objects = models.Manager()
    objects       = ArchiveManager()


    class Meta:
        app_label           = "league"
        db_table            = "allstar_league_season"
        ordering            = ['-year']
        verbose_name        = 'Season'
        verbose_name_plural = 'Seasons'
        get_latest_by       = 'start_date'

    def __str__( self ):
        return "%s season" % self.year

    def __unicode__(self):
        """

        """
        return u'%s' % self.year

