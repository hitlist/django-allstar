from django.conf.urls import include, url
from django.core.validators import slug_re
from django.template.defaultfilters import slugify
from .views.league import LeagueList, LeagueDetail, LeagueTeamList
from .views.team import TeamView
from .api.position import (PositionResource, PositionDetailResource)
from .api.league import (LeagueMemberResource, LeagueListResource)

res = {
    'ct': r'(?P<content_type>[a-z][a-z0-9-]+)',
    'year': r'(?P<year>\d{4})',
    'month': r'(?P<month>\d{1,2})',
    'day': r'(?P<day>\d{1,2})',
    'rest': r'(?P<url_remainder>.+/)',
    'slug': r'(?P<slug>%s)' % slug_re.pattern.strip('^$'),
    'pk': r'(?P<pk>[0-9a-f-]+)',
    'ct_id': r'(?P<ct_id>\d+)',
}

urlpatterns = [
  url(r'', include([
    url(r'^league$', LeagueList.as_view(), name='allstar-league-list'),
    url(r'^league/%(pk)s/%(ct)s-%(ct_id)s/%(slug)s$' % res, LeagueList.as_view(), name='allstar-league-detail'),
    url(r'^league/%(pk)s/%(ct)s-%(ct_id)s/(?P<slug>\w+)/teams/%(year)s$' % res, LeagueTeamList.as_view(), name='allstar-league-list-year'),
    url(r'^league/%(ct)s-%(ct_id)s/%(pk)s/%(slug)s$' % res, TeamView.as_view(), name='allstar-league-team-detail'),
  ])),

  url(r'^api/', include([
    url(r'^position$', PositionResource.as_view(), name='allstar-league-api-position-list'),
    url(r'^position/%(pk)s' % res, PositionDetailResource.as_view(), name='allstar-league-api-position-detail'),
    url(r'^league$', LeagueListResource.as_view(), name='allstar-league-api-league-list'),
    url(r'^league/%(pk)s/team' % res, LeagueMemberResource.as_view(), name='allstar-league-api-league-detail')
  ])),
]


