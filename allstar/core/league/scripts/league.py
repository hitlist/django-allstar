#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been automatically generated.
# Instead of changing it, create a file called import_helper.py
# and put there a class called ImportHelper(object) in it.
#
# This class will be specially casted so that instead of extending object,
# it will actually extend the class BasicImportHelper()
#
# That means you just have to overload the methods you want to
# change, leaving the other ones inteact.
#
# Something that you might want to do is use transactions, for example.
#
# Also, don't forget to add the necessary Django imports.
#
# This file was generated with the following command:
# ./manage.py dumpscript league.Season
#
# to restore it, run
# manage.py runscript module_name.this_script_name
#
# example: if manage.py is at ./manage.py
# and the script is at ./some_folder/some_script.py
# you must make sure ./some_folder/__init__.py exists
# and run  ./manage.py runscript some_folder.some_script
import os, sys
from math import floor
from random import choice, randint, seed
from django.db import transaction
from itertools import product

class BasicImportHelper(object):

    def pre_import(self):
        pass

    @transaction.atomic
    def run_import(self, import_data):
        import_data()

    def post_import(self):
        pass

    def locate_similar(self, current_object, search_data):
        # You will probably want to call this method from save_or_locate()
        # Example:
        #   new_obj = self.locate_similar(the_obj, {"national_id": the_obj.national_id } )

        the_obj = current_object.__class__.objects.get(**search_data)
        return the_obj

    def locate_object(self, original_class, original_pk_name, the_class, pk_name, pk_value, obj_content):
        # You may change this function to do specific lookup for specific objects
        #
        # original_class class of the django orm's object that needs to be located
        # original_pk_name the primary key of original_class
        # the_class      parent class of original_class which contains obj_content
        # pk_name        the primary key of original_class
        # pk_value       value of the primary_key
        # obj_content    content of the object which was not exported.
        #
        # You should use obj_content to locate the object on the target db
        #
        # An example where original_class and the_class are different is
        # when original_class is Farmer and the_class is Person. The table
        # may refer to a Farmer but you will actually need to locate Person
        # in order to instantiate that Farmer
        #
        # Example:
        #   if the_class == SurveyResultFormat or the_class == SurveyType or the_class == SurveyState:
        #       pk_name="name"
        #       pk_value=obj_content[pk_name]
        #   if the_class == StaffGroup:
        #       pk_value=8

        search_data = { pk_name: pk_value }
        the_obj = the_class.objects.get(**search_data)
        #print(the_obj)
        return the_obj


    def save_or_locate(self, the_obj):
        # Change this if you want to locate the object in the database
        try:
            the_obj.save()
        except:
            print("---------------")
            print("Error saving the following object:")
            print(the_obj.__class__)
            print(" ")
            print(the_obj.__dict__)
            print(" ")
            print(the_obj)
            print(" ")
            print("---------------")

            raise
        return the_obj


importer = None
try:
    import import_helper
    # We need this so ImportHelper can extend BasicImportHelper, although import_helper.py
    # has no knowlodge of this class
    importer = type("DynamicImportHelper", (import_helper.ImportHelper, BasicImportHelper ) , {} )()
except ImportError as e:
    # From Python 3.3 we can check e.name - string match is for backward compatibility.
    if 'import_helper' in str(e):
        importer = BasicImportHelper()
    else:
        raise

import datetime
from decimal import Decimal
from django.contrib.contenttypes.models import ContentType

try:
    import dateutil.parser
except ImportError:
    print("Please install python-dateutil")
    sys.exit(os.EX_USAGE)

def run():
    importer.pre_import()
    importer.run_import(import_data)
    importer.post_import()

def import_data():
    # Initial Imports

    # Processing model: allstar.core.league.models.league.Season
    import datetime
    from uuid import uuid4 as UUID
    from allstar.core.league.models import Season, Game, League, Stadium, LeagueTeam, Conference
    from allstar.core.league.models.team import Stadium, Team
    from allstar.core.darkroom.models import Photo
    from allstar.contrib.authentication.models import Allstar

    leagues = []
    seasons = []
    conferences = []
    allstar_league_conference_1 = Conference()
    allstar_league_conference_1.archived = None
    allstar_league_conference_1.created_at = dateutil.parser.parse("2017-08-13T17:51:28.109078+00:00")
    allstar_league_conference_1.updated_at = dateutil.parser.parse("2017-08-13T17:51:28.109091+00:00")
    allstar_league_conference_1.id = 'd28201f8-0612-450d-86dd-bc9f9a9207a6'
    allstar_league_conference_1.name = 'National'
    allstar_league_conference_1.logo =  importer.locate_object(Photo, "id", Photo, "id", "363d0a6b-5624-4a8a-b85a-33f8cbd9f901", {"archived": None, "created_at": datetime.datetime(2017, 8, 13, 17, 51, 25, 990375), "updated_at": datetime.datetime(2017, 8, 13, 17, 51, 25, 990391), "id": "363d0a6b-5624-4a8a-b85a-33f8cbd9f901", "title": "National Conference", "description": "", "image": "photos/national-conference-logo0.5x.png"} )
    allstar_league_conference_1.slug = 'national'
    allstar_league_conference_1 = importer.save_or_locate(allstar_league_conference_1)
    conferences.append(allstar_league_conference_1)

    allstar_league_conference_2 = Conference()
    allstar_league_conference_2.archived = None
    allstar_league_conference_2.created_at = dateutil.parser.parse("2017-08-13T17:51:58.882658+00:00")
    allstar_league_conference_2.updated_at = dateutil.parser.parse("2017-08-13T17:51:58.882674+00:00")
    allstar_league_conference_2.id = '90474418-bf92-49fd-9250-db848c81ed39'
    allstar_league_conference_2.name = 'American'
    allstar_league_conference_2.logo =  importer.locate_object(Photo, "id", Photo, "id", "2632eb2b-8617-4244-b02f-2145f322b6e0", {"archived": None, "created_at": datetime.datetime(2017, 8, 13, 17, 51, 56, 409424), "updated_at": datetime.datetime(2017, 8, 13, 17, 51, 56, 409436), "id": "2632eb2b-8617-4244-b02f-2145f322b6e0", "title": "American Conference Logo", "description": "", "image": "photos/ifl-american-conference.png"})
    allstar_league_conference_2.slug = 'american'
    allstar_league_conference_2 = importer.save_or_locate(allstar_league_conference_2)
    conferences.append(allstar_league_conference_2)


    allstar_league_stadium_1 = Stadium()
    allstar_league_stadium_1.archived = None
    allstar_league_stadium_1.created_at = dateutil.parser.parse("2017-07-21T11:41:28.424616+00:00")
    allstar_league_stadium_1.updated_at = dateutil.parser.parse("2017-07-21T11:41:28.424639+00:00")
    allstar_league_stadium_1.id = '575f05ce-539c-42e8-824e-2f05bce8d033'
    allstar_league_stadium_1.name = 'Inpro Stadium'
    allstar_league_stadium_1.address = '123 Main st'
    allstar_league_stadium_1.city = 'Muskego'
    allstar_league_stadium_1.state = 'WI'
    allstar_league_stadium_1.zip_code = '53150'
    allstar_league_stadium_1.field_type = 1
    allstar_league_stadium_1.lights = True
    allstar_league_stadium_1.consessions = True
    allstar_league_stadium_1.slug = 'inpro-stadium'
    allstar_league_stadium_1.latitude = None
    allstar_league_stadium_1.longitude = None
    allstar_league_stadium_1 = importer.save_or_locate(allstar_league_stadium_1)


    allstar_league_season_1 = Season()
    allstar_league_season_1.archived = None
    allstar_league_season_1.created_at = dateutil.parser.parse("2017-07-15T20:43:06.257000+00:00")
    allstar_league_season_1.updated_at = dateutil.parser.parse("2017-07-15T20:43:06.257000+00:00")
    allstar_league_season_1.year = 2017
    allstar_league_season_1 = importer.save_or_locate(allstar_league_season_1)
    
    allstar_league_season_2 = Season()
    allstar_league_season_2.archived = None
    allstar_league_season_2.created_at = dateutil.parser.parse("2017-07-18T03:27:10.568943+00:00")
    allstar_league_season_2.updated_at = dateutil.parser.parse("2017-07-18T03:27:10.568967+00:00")
    allstar_league_season_2.year = 2016
    allstar_league_season_2 = importer.save_or_locate(allstar_league_season_2)

    allstar_league_season_3 = Season()
    allstar_league_season_3.archived = None
    allstar_league_season_3.created_at = dateutil.parser.parse("2017-07-18T03:34:55.678490+00:00")
    allstar_league_season_3.updated_at = dateutil.parser.parse("2017-07-18T03:34:55.678508+00:00")
    allstar_league_season_3.year = 2015
    allstar_league_season_3 = importer.save_or_locate(allstar_league_season_3)
    print('seasons created')
    allstar_league_1 = League()
    allstar_league_1.archived = None
    allstar_league_1.created_at = dateutil.parser.parse("2017-07-15T20:43:35.606000+00:00")
    allstar_league_1.updated_at = dateutil.parser.parse("2017-07-15T20:43:35.606000+00:00")
    allstar_league_1.id = 'bfee93be-8432-48ba-8cac-1afb159e8b09'
    allstar_league_1.name = 'Ironman Football League'
    allstar_league_1.abbreviation = 'IFL'
    allstar_league_1.decommision_date = None
    allstar_league_1.founded = dateutil.parser.parse("2001-11-01")
    allstar_league_1 = importer.save_or_locate(allstar_league_1)

    allstar_league_2 = League()
    allstar_league_2.archived = None
    allstar_league_2.created_at = dateutil.parser.parse("2017-07-18T03:27:01.126919+00:00")
    allstar_league_2.updated_at = dateutil.parser.parse("2017-07-18T03:27:01.126954+00:00")
    allstar_league_2.id = 'dba45031-0bec-4c0c-b207-285494a4fe86'
    allstar_league_2.name = 'North American Football League'
    allstar_league_2.abbreviation = 'NAFL'
    allstar_league_2.decommision_date = None
    allstar_league_2.founded = dateutil.parser.parse("2001-01-01")
    allstar_league_2 = importer.save_or_locate(allstar_league_2)

    founder = Allstar()
    founder.password = 'pbkdf2_sha256$36000$mhwZmaTyN58b$pal/idUZN+xvyG5gSSLJsIbQrp4H8yVV1gYG1A6j//s='
    founder.username = 'esatterwhite'
    founder.first_name = ''
    founder.is_staff = True
    founder.last_name = ''
    founder._password = None
    founder.is_active = True
    founder.is_superuser = True
    founder.email = 'esatterwhite@wi.rr.com'
    founder.last_login = datetime.datetime(2017, 7, 18, 3, 24, 49, 168732)
    founder.date_joined: datetime.datetime(2017, 7, 17, 12, 22, 26, 474105)
    founder = importer.save_or_locate(founder)

    # Re-processing model: allstar.core.league.models.league.League
    allstar_league_1.founders.add( founder )

    allstar_league_2.founders.add( founder )
    print('leagues created')
    leagues  = [allstar_league_season_1, allstar_league_season_2, allstar_league_season_3]
    seasons = [allstar_league_1, allstar_league_2]
    try:
        teams = LeagueTeam.objects.bulk_create([
            LeagueTeam(season=x[1], league=x[0], team=x[2], stadium=allstar_league_stadium_1, conference=choice(conferences))
            for x in product(seasons, leagues, Team.objects.all())
        ])

    except Exception as e:
        print(e)
        pass

    print('league teams created')
    for args in product(seasons, leagues):
        generate_games(*args)


def generate_games(league, season):
    seed(randint(0,1000))
    from allstar.core.league.models import Game, Team, LeagueTeam
    from datetime import datetime, timedelta
    now = datetime(season.pk, 6, 1)
    teams = LeagueTeam.objects.filter(league=league, season=season)
    games = []
    for week in range(15):
        for team in range(floor(len(teams)/2)):
            home = choice(teams)
            away = choice(teams)
            while home.id == away.id:
                away = choice(teams)

            games.append(
                Game(
                    home_team = home,
                    away_team = away,
                    home_score = randint(2, 70),
                    away_score = randint(2, 70),
                    date_played = now,
                    status = 2
                )
            )
        now = now + timedelta(weeks=1)
    print('generating {} games'.format(len(games)))
    Game.objects.bulk_create(games)



