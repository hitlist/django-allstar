from django import template

register = template.Library()

@register.filter
def record(values):
    return '{:d} - {:d} - {:d}'.format(*values)
