import random

from uuid import uuid4
from django.db import models
from django.conf import settings
from django.db.models import Q
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

from imagekit.models import ImageSpecField
from imagekit.processors import SmartResize, ResizeToFit

from allstar.core.models import TimeStampedModel
from allstar.core.managers import ArchiveManager

DARKROOM_THUMBNAIL_WIDTH = getattr(settings, 'DARKROOM_THUMBNAIL_WIDTH', 80)
DARKROOM_THUMBNAIL_HEIGHT = getattr(settings, 'DARKROOM_THUMBNAIL_HEIGHT', 80)
DARKROOM_THUMBNAIL = (DARKROOM_THUMBNAIL_WIDTH, DARKROOM_THUMBNAIL_HEIGHT)

DARKROOM_AVATAR_WIDTH = getattr(settings, 'DARKROOM_AVATAR_WIDTH', 300)
DARKROOM_AVATAR_HEIGHT = getattr(settings, 'DARKROOM_AVATAR_HEIGHT', 150)
DARKROOM_AVATAR = (DARKROOM_AVATAR_WIDTH, DARKROOM_AVATAR_HEIGHT)

DARKROOM_LARGE_WIDTH = getattr(settings, 'DARKROOM_LARGE_WIDTH', 500)
DARKROOM_LARGE_HEIGHT = getattr(settings, 'DARKROOM_LARGE_HEIGHT', 500)
DARKROOM_LARGE = (DARKROOM_LARGE_WIDTH, DARKROOM_LARGE_HEIGHT)

DARKROOM_MEDIUM_WIDTH = getattr(settings, 'DARKROOM_MEDIUM_WIDTH', 280)
DARKROOM_MEDIUM_HEIGHT = getattr(settings, 'DARKROOM_MEDIUM_HEIGHT', 215)
DARKROOM_MEDIUM = (DARKROOM_MEDIUM_WIDTH, DARKROOM_MEDIUM_HEIGHT)

DARKROOM_BLOG_WIDTH = getattr(settings, 'DARKROOM_BLOG_WIDTH', 215)
DARKROOM_BLOG_HEIGHT = getattr(settings, 'DARKROOM_BLOG_HEIGHT', 150)
DARKROOM_BLOG = (DARKROOM_BLOG_WIDTH, DARKROOM_BLOG_HEIGHT)

DARKROOM_HEADLINE_WIDTH = getattr(settings, 'DARKROOM_HEADLINE_WIDTH', 150)
DARKROOM_HEADLINE_HEIGHT = getattr(settings, 'DARKROOM_HEADLINE_HEIGHT', 150)
DARKROOM_HEADLINE = (DARKROOM_HEADLINE_WIDTH, DARKROOM_HEADLINE_HEIGHT)

DARKROOM_FEATURE_WIDTH = getattr(settings, 'DARKROOM_FEATURE_WIDTH', 150)
DARKROOM_FEATURE_HEIGHT = getattr(settings, 'DARKROOM_FEATURE_HEIGHT', 65)
DARKROOM_FEATURE = (DARKROOM_FEATURE_WIDTH, DARKROOM_FEATURE_HEIGHT)

DARKROOM_WIDETHUMB_WIDTH = getattr(settings, 'DARKROOM_WIDETHUMB_WIDTH', 140)
DARKROOM_WIDETHUMB_HEIGHT = getattr(settings, 'DARKROOM_WIDETHUMB_HEIGHT', 100)
DARKROOM_WIDETHUMB = (DARKROOM_WIDETHUMB_WIDTH, DARKROOM_WIDETHUMB_HEIGHT)

DARKROOM_WIDETHUMB_WIDTH = getattr(settings, 'DARKROOM_WIDETHUMB_WIDTH', 140)
DARKROOM_WIDETHUMB_HEIGHT = getattr(settings, 'DARKROOM_WIDETHUMB_HEIGHT', 100)
DARKROOM_WIDETHUMB = (DARKROOM_WIDETHUMB_WIDTH, DARKROOM_WIDETHUMB_HEIGHT)

DARKROOM_SMALL_WIDTH = getattr(settings, 'DARKROOM_SMALL_WIDTH', 100)
DARKROOM_SMALL_HEIGHT = getattr(settings, 'DARKROOM_SMALL_HEIGHT', 100)
DARKROOM_SMALL = (DARKROOM_SMALL_WIDTH, DARKROOM_SMALL_HEIGHT)

DARKROOM_MINITHUMB_WIDTH = getattr(settings, 'DARKROOM_MINITHUMB_WIDTH', 70)
DARKROOM_MINITHUMB_HEIGHT = getattr(settings, 'DARKROOM_MINITHUMB_HEIGHT', 50)
DARKROOM_MINITHUMB = (DARKROOM_MINITHUMB_WIDTH, DARKROOM_MINITHUMB_HEIGHT)

DARKROOM_ICON_WIDTH = getattr(settings, 'DARKROOM_ICON_WIDTH', 45)
DARKROOM_ICON_HEIGHT = getattr(settings, 'DARKROOM_ICON_HEIGHT', 45)
DARKROOM_ICON = (DARKROOM_ICON_WIDTH, DARKROOM_ICON_HEIGHT)


class Photo(TimeStampedModel):
    '''
    Model representing a Photo which knows how to generate multiple versions
    and sizes of itself.
    '''
    id = models.UUIDField(
        _('id'),
        db_column='photo_id',
        primary_key=True,
        db_index=True,
        editable=False,
        default=uuid4
    )

    title = models.CharField(
        verbose_name=_(u'Title'),
        max_length=255,
        blank=True,
        null=True
    )

    description = models.TextField(
        blank=True,
        null=True,
        default=None
    )

    image = models.ImageField(
        upload_to='photos/'
    )

    thumbnail = ImageSpecField(
        source='image',
        options={'quality': 65},
        processors=[SmartResize(*DARKROOM_THUMBNAIL)]
    )

    avatar = ImageSpecField(
        source='image',
        options={'quality': 80},
        processors=[ResizeToFit(*DARKROOM_AVATAR)]
    )

    medium = ImageSpecField(
        source='image',
        options={'quality': 70},
        processors=[SmartResize(*DARKROOM_MEDIUM)]
    )

    large = ImageSpecField(
        source='image',
        options={'quality': 85},
        processors=[SmartResize(*DARKROOM_LARGE)]
    )

    blog = ImageSpecField(
        source='image',
        options={'quality': 85},
        processors=[SmartResize(*DARKROOM_BLOG)]
    )

    headline = ImageSpecField(
        source='image',
        options={'quality': 75},
        processors=[SmartResize(*DARKROOM_HEADLINE)]
    )

    feature = ImageSpecField(
        source='image',
        options={'quality': 85},
        processors=[SmartResize(*DARKROOM_FEATURE)]
    )

    widethumb = ImageSpecField(
        source='image',
        options={'quality': 85},
        processors=[SmartResize(*DARKROOM_WIDETHUMB)]
    )

    small = ImageSpecField(
        source='image',
        options={'quality': 60},
        processors=[SmartResize(*DARKROOM_SMALL)]
    )

    mini = ImageSpecField(
        source='image',
        options={'quality': 85},
        processors=[SmartResize(*DARKROOM_MINITHUMB)]
    )

    icon = ImageSpecField(
        source='image',
        options={'quality': 50},
        processors=[SmartResize(*DARKROOM_ICON)]
    )

    admin_objects = models.Manager()
    objects = ArchiveManager()

    class Meta:
        verbose_name = _('Photo')
        verbose_name_plural = _('Photos')
        get_latest_by = 'created_at'

    def __str__(self):
        return str(self.title)

    def __unicode(self):
        return self.title


class Gallery(TimeStampedModel):
    '''
    Holds a collection of photo objects for viewing
    '''
    id = models.UUIDField(
        _('id'),
        db_column='gallery_id',
        primary_key=True,
        db_index=True,
        editable=False,
        default=uuid4
    )

    photos = models.ManyToManyField(
        Photo,
        verbose_name=_(u'Photos'),
        help_text=_(u'Photos to include in this gallery'),
        related_name='galleries'
    )

    title = models.CharField(
        verbose_name=_(u'title'),
        max_length=80,
        blank=False,
        null=False
    )

    slug = models.SlugField(
        blank=True,
        null=False,
        editable=False
    )

    description = models.TextField(
        _(u'Description'),
        blank=True,
        help_text=_(u'A description of this Gallery.')
    )

    feature = models.BooleanField(
        ('Featured'),
        default=False,
        help_text=_(u'Featured Galleries will be displayed On the home page')
    )

    admin_objects = models.Manager()
    objects = ArchiveManager()

    class Meta:
        get_latest_by = 'created_at'
        verbose_name = _(u'gallery')
        verbose_name_plural = _(u'galleries')

    def __unicdoe__(self):
        return self.title

    def random(self):
        '''
        Returns a random image from the related photos
        '''
        photo_set = self.photos.filter(Q(archived__is_null=True))
        return random.sample(set(photo_set), 1)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super(Gallery, self).save(*args, **kwargs)
