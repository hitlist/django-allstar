from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
class LoginForm( forms.Form ):
	username = forms.CharField(max_length=225)
	password = forms.CharField(max_length=225)

class UserForm( forms.ModelForm ):

	class Meta:
		model =get_user_model()
		verbose_name=_(u"User Form")


