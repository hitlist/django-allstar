from datetime import datetime
from django.db import models
from django.core.cache import cache
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _


class ModelBase(models.Model):
    class Meta:
        abstract = True

    archived = models.DateTimeField(verbose_name=_(u'archived'), null = True, default=None, blank=True)

    def get_ct( self ):
        return ContentType.objects.get_for_model( self )

    def get_ct_id( self ):
        return self.get_ct().pk

    def get_app_label( self ):
        return self.get_ct().app_label

    def get_model_name( self ):
        return u'%s' % self.get_ct().model

    def get_class_name( self ):
        return self._meta.verbose_name

    def get_cache_key( self, key=None ):
        ct = self.get_ct()
        schema_name = connection.get_schema
        cache_key =  "%s:%s:%s:%s" %( schema_name, ct.app_label, ct.model, self.pk )

        cache_key = cache_key + ":%s" % key if key is not None else cache_key

        return cache_key

    def cache_get( self, key ):
        """
            returns cached value for a given key, by model for a tenant
        """
        cache_key = self.get_cache_key(key)
        return cache.get( cache_key )

    def cache_set(self, key, value):
        """
            sets a value at a known key for a specific model instance on a tenant
        """
        cache_key = self.get_cache_key(key)
        cache.set( cache_key, value, 60 * 60 * 1)

    @property
    def pk_field( self ):
        return getattr( self._meta, "pk").verbose_name

    @property
    def count( self ):
        return cache.get( self.count_key( "count" ) )


class TimeStampedModel(ModelBase):

    class Meta:
        abstract = True

    created_at = models.DateTimeField(null = False, auto_now_add = True)
    updated_at = models.DateTimeField(null = False, auto_now = True)
