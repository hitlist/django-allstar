import graphene
from graphene_django import DjangoObjectType
from django.contrib.auth import get_user_model
from allstar.core.league.models import Leage

User = get_user_model()

class Query(qraphene.ObjectType):
    users = graphene.List(User)

    def resolve_users(self, info):
        print(info)
        return User.objects.all()
