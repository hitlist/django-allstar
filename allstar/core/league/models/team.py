import datetime
from uuid import uuid4
from django.db import models
from django.db.models import Q, F, Sum, Value, Case, When, Count, IntegerField
from django.db.models.functions import Coalesce
from django.conf import settings
from django.utils.text import slugify
from django.utils.functional import cached_property
from django.urls import reverse
from django.contrib.auth import get_user_model
from allstar.core.models import TimeStampedModel, ModelBase
from allstar.core.managers import ArchiveManager
from django.utils.translation import ugettext_lazy as _
from allstar.core.darkroom.models import Photo
from allstar.core.constants import STATES
from .game import Game, RushingStatistic
from ..constants import (
    STADIUM_FIELD_TYPES, GAME_TYPE_PRESEASON, GAME_TYPE_REGULARSEASON,
    GAME_TYPE_PLAYOFF, GAME_TYPE_CHAMPIONSHIP, GAME_STATUS_PLAYED
)

NON_PRESEASON = ~Q(game_type=GAME_TYPE_PRESEASON)
CHAMPIONSHIP_GAME = Q(game_type=GAME_TYPE_CHAMPIONSHIP)
PLAYOFF_GAME  = Q(game_type=GAME_TYPE_PLAYOFF)
PRESEASON_GAME = Q(game_type=GAME_TYPE_PRESEASON)
REGULAR_GAME = Q(game_type=GAME_TYPE_REGULARSEASON)
GAME_PLAYED = Q(status=GAME_STATUS_PLAYED)

def _games_for_season(self, games, season):
    home_team_season = Q(home_team__team=self) & Q(home_team__season=season)
    away_team_season = Q(away_team__team=self) & Q(away_team__season=season)
    return games.filter(home_team_season | away_team_season)

def _games_for_league(self, games, league):
    home_team_league = Q(home_team__team=self) & Q(home_team__league=league)
    away_team_league = Q(away_team__team=self) & Q(away_team__league=league)
    return games.filter(away_team_league | home_team_league)

def _stats_for_season(self, stats, season):
    return stats.filter(player__team__season=season)

def _stats_for_league(self, stats, league):
    return stats.filter(player__team__league=league)


class Stadium(TimeStampedModel):
    '''
    Represents a physical location where games take place
    '''
    id            = models.UUIDField(db_column='stadium_id', primary_key=True, db_index=True, default=uuid4, editable=False)
    name          = models.CharField(verbose_name=_(u'Stadium Name'), max_length=50, db_column='stadium_name')
    address       = models.CharField(max_length=50)
    city          = models.CharField(max_length=50)
    state         = models.CharField(max_length=2, choices=STATES)
    zip_code      = models.CharField(max_length=5)
    field_type    = models.IntegerField(choices=STADIUM_FIELD_TYPES)
    lights        = models.BooleanField()
    consessions   = models.BooleanField()
    slug          = models.SlugField(blank=True, null=False, editable=False)
    latitude      = models.DecimalField(blank=True, null=True, max_digits=9, decimal_places=6)
    longitude     = models.DecimalField(blank=True, null=True, max_digits=9, decimal_places=6)


    admin_objects = models.Manager()
    objects       = ArchiveManager()


    class Meta:
        app_label = 'league'
        db_table = 'allstar_league_stadium'
        abstract = False

    def __str__(self):
      return self.name
    def __unicode__(self):
        return unicode(self.__str__())

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(Stadium, self).save(*args, **kwargs)

class TeamRivalry(ModelBase):
    id            = models.UUIDField(db_column='league_team_id', primary_key=True, db_index=True, default=uuid4, editable=False )
    league        = models.ForeignKey( 'League', verbose_name=_(u'League'), on_delete=models.CASCADE)
    season        = models.ForeignKey( 'Season', verbose_name=_(u'Season'), on_delete=models.CASCADE)
    rival_of      = models.ForeignKey( 'Team', verbose_name=_(u'Rival'), related_name='rival_of', on_delete=models.CASCADE)
    rival_to      = models.ForeignKey( 'Team', verbose_name=_(u'Rival'), related_name='rival_to', on_delete=models.CASCADE)

    admin_objects = models.Manager()
    objects       = ArchiveManager()

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_rivalry'
        unique_together     =( ( 'season', 'league', 'rival_of', 'rival_to'), )
        verbose_name        = 'Team Rivalry'
        verbose_name_plural = 'Team Rivalries'

    def __unicode__( self ):
        return '( Rivalry ) %s vs %s ' %( self.rival_to, self.rival_of )

class Position( TimeStampedModel ):
    id            = models.UUIDField(db_column='position_id', primary_key=True, db_index=True, default=uuid4, editable=False )
    abbr          = models.CharField(db_column='position_abbreviation', max_length=25, db_index=True, unique=True)
    title         = models.CharField(db_column='position_title', max_length=50, null=False, blank=False)
    category      = models.CharField(db_column='position_category', max_length=100, null=False, blank=False)

    admin_objects           = models.Manager()
    objects                 = ArchiveManager()

    class Meta:
        app_label           = 'league'
        db_table            = 'allstar_league_position'
        verbose_name        = _( u'Position' )
        verbose_name_plural = _( u'Positions' )

    def __str__( self ):
        return self.title

    def __unicde__( self ):
        return unicode(self.__str__())

class TeamMember( TimeStampedModel ):
    id            = models.UUIDField( db_column='team_member_id', primary_key=True, db_index=True, default=uuid4, editable=False )
    team          = models.ForeignKey('LeagueTeam', verbose_name=_(u'Team'), on_delete=models.CASCADE)
    user          = models.ForeignKey(get_user_model(), verbose_name=_('User'), on_delete=models.CASCADE )
    number        = models.PositiveSmallIntegerField(_('Number'), default=None, blank=True, null=True)
    position      = models.ForeignKey(Position, verbose_name=_(u'Position'), blank=False, null=False, on_delete=models.CASCADE)
    admin_objects = models.Manager()
    objects       = ArchiveManager()


    class Meta:
        app_label = 'league'
        db_table  = 'allstar_league_team_member'
        verbose_name = _(u'Team Member')
        verbose_name_plural = _(u'Team Members')
        unique_together = ('team', 'number')

    def __repr__(self):
        return '<TeamMembership: {0} {1}>'.format(self.team.season.pk, self.user)

    def __str__( self ):
        return '#%s %s %s: %s' % ( self.number, self.user.get_full_name(), self.team.team.name, self.team.season )

    @staticmethod
    def autocomplete_search_fields():
        return ('position__title__icontains', 'user__first_name__icontains', 'user__last_name__icontains', 'team__name__icontains',)

class Team( TimeStampedModel ):
    id                    = models.UUIDField( db_column='team_id', primary_key=True, db_index=True, default=uuid4, editable=False )
    name                  = models.CharField( max_length = 20, blank =False, db_column='team_name',  help_text = 'Name Related to team mascot i.e. Patriots or Packers')
    year_established      = models.DateField( 'Year Established', blank=False, help_text='YYYY-MM-DD')
    nickname              = models.CharField( max_length = 3, help_text='3 letter Abbreviation', db_column='team_nickname')
    bio                   = models.TextField( 'Bio',blank=True, null=True, db_column='team_bio' )
    city                  = models.CharField( max_length = 20, blank=False, db_column='team_city')
    state                 = models.CharField( _('State'), choices=STATES, max_length=2)
    team_rivals           = models.ManyToManyField( 'self', symmetrical=False, blank=True, null=True, related_name='rival_set', through=TeamRivalry)
    website               = models.URLField( blank=True, db_column='team_website' )
    primary_team          = models.NullBooleanField( help_text = 'Is this team Your team?', unique=True )
    slug                  = models.SlugField( blank=True, editable=False)
    team_code             = models.CharField( 'Team Code', max_length=100, blank=True, null=True, db_index=True )
    logo                  = models.ForeignKey( Photo, blank=True, null=True, verbose_name=_(u'team logo'), db_column='team_logo', on_delete=models.CASCADE)
    helmet_graphic_home   = models.ForeignKey( Photo, verbose_name=_(u'Helmet Graphic - Home') , help_text=_(u'Graphic used for home games'), related_name='photo_home_set', on_delete=models.CASCADE )
    helmet_graphic_away   = models.ForeignKey( Photo, verbose_name=_(u'Helment Graphic - Away') , help_text=_(u'Graphic used for away Games'), related_name='photo_away_set', on_delete=models.CASCADE)
    admin_objects         = models.Manager()
    objects               = ArchiveManager()

    class Meta:
        app_label = 'league'
        db_table = 'allstar_league_team'
        abstract = False

    def save(self, *args, **kwargs):
        self.slug = slugify('{0} {1}'.format(self.city,self.name))
        return super(Team, self).save(*args, **kwargs)

    def __repr__( self ):
        return '<Team: %s >' % self.name

    def __str__( self ):
        return '%s' % self.full_name

    def __unicode__( self ):
        return str( self ).encode('utf-8')

    def __eq__( self, team ):
        if isinstance(team, Team):
            return self.winning_percent() == team.winning_percent()
        return False

    def __ne__( self, team ):
        if isinstance(team, Team):
            return self.winning_percent() != team.winning_percent()
        return True

    def __gt__( self, team ):
        if isinstance(team, Team):
            return self.winning_percent() > team.winning_percent()
        return True

    def __ge__( self, team ):
        if isinstance(team, Team):
            return self.winning_percent() >= team.winning_percent()
        return True

    def __lt__( self, team ):
        if isinstance(team, Team):
            return self.winning_percent() < team.winning_percent()
        return False

    def __le__( self, team ):
        if isinstance(team, Team):
            return self.winning_percent() <= team.winning_percent()
        return False


    def get_absolute_url(self):
        return reverse('allstar-league-team-detail', kwargs ={
          'pk': str(self.pk)
        , 'content_type': self.get_ct().name
        , 'ct_id': self.get_ct_id()
        , 'slug': self.slug
        })

    @property
    def full_name( self ):
        return '%s %s' % (self.city, self.name)

    def record(self, season = None, league = None):
        games = Game.objects.filter(GAME_PLAYED).filter(Q(home_team__team=self) | Q(away_team__team=self))

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team__pk=self.pk)

        away_losses = Q(home_score__lt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_losses = Q(away_score__lt=F('home_score')) & Q(away_team__team__pk=self.pk)

        away_ties = Q(home_score=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_ties = Q(away_score=F('home_score')) & Q(away_team__team__pk=self.pk)

        result = games.aggregate(
            wins = Sum(
                Case(
                  When(home_wins | away_wins, then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),
            losses = Sum(
                Case(
                  When(home_losses | away_losses, then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),

            ties = Sum(
                Case(
                  When(home_ties | away_ties, then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),

        )
        return (result['wins'] or 0, result['losses'] or 0, result['ties'] or 0)

    def season_record(self, season, league = None):
        return self.wins(season=season, league=league)

    def season_record_str(self, season, league = None):
        return '%d-%d-%d' % self.wins(season=season, league=league)

    def league_record(self, league, season=None):
        return self.record(season=season, league=league)

    def league_record_str( self, league, season=None):
        return '%d-%d-%d' % self.record(season = season, leauge = league)

    def winning_percent( self, season = None, league = None ):
        home_team = Q(home_team__team=self)
        away_team = Q(away_team__team=self)
        games = Game.objects.filter(GAME_PLAYED).filter(home_team | away_team)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        away_wins = Q(home_score__gt=F('away_score')) & home_team
        home_wins = Q(away_score__gt=F('home_score')) & away_team

        result = games.aggregate(
          wins = Sum(
            Case(
              When(home_wins | away_wins, then=Value(1))
            , default=Value(0), output_field=IntegerField()
            )
          )
        , games = Sum(
            Case(
              When(home_team | away_team, then=Value(1))
            , default=Value(0), output_field=IntegerField()
            )
          )
        )
        wins = result['wins'] or 0
        games = result['games'] or 0
        if games == 0:
            return 0

        return float(wins / games)

    def wins_against(self, team, season = None, league = None):
        home_wins = Q(home_team__team=self, away_team__team=team, home_score__gt=F('away_score'))
        away_wins = Q(home_team__team=team, away_team__team=self, away_score__gt=F('home_score'))
        games = Game.objects.filter(GAME_PLAYED).filter(home_wins | away_wins)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def losses_against(self, team, season = None, league = None):
        home_losses = Q(home_team__team=self, away_team__team=team, home_score__lt=F('away_score'))
        away_losses = Q(home_team__team=team, away_team__team=self, away_score__lt=F('home_score'))
        games = Game.objects.filter(GAME_PLAYED).filter(home_losses | away_losses)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def ties_against(self, team, season = None, league = None):
        home_ties = Q(home_team__team=self, away_team__team=team, home_score=F('away_score'))
        away_ties = Q(home_team__team=team, away_team__team=self, away_score=F('home_score'))
        games = Game.objects.filter(GAME_PLAYED).filter(home_ties | away_ties)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def record_against(self, team, season = None, league = None):
        home_team = Q(home_team__team=self, away_team__team=team)
        away_team = Q(home_team__team=team, away_team__team=self)

        games = Game.objects.filter(GAME_PLAYED).filter(home_team | away_team)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        home_wins = Q(home_team__team=self, away_team__team=team, home_score__gt=F('away_score'))
        away_wins = Q(home_team__team=team, away_team__team=self, away_score__gt=F('home_score'))

        home_losses = Q(home_team__team=self, away_team__team=team, home_score__lt=F('away_score'))
        away_losses = Q(home_team__team=team, away_team__team=self, away_score__lt=F('home_score'))

        home_ties = Q(home_team__team=self, away_team__team=team, home_score=F('away_score'))
        away_ties = Q(home_team__team=team, away_team__team=self, away_score=F('home_score'))

        result = games.aggregate(
            wins = Sum(
                Case(
                  When(home_wins | away_wins, then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),
            losses = Sum(
                Case(
                  When(home_losses | away_losses, then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),

            ties = Sum(
                Case(
                  When(home_ties | away_ties, then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),
        )

        return (
          result['wins'] or 0
        , result['losses'] or 0
        , result['ties'] or 0
        )

    def wins(self, season = None, preseason = False, league = None):
        if preseason:
            return self.preseason_wins(season = season, league=None)

        return self.alltime_wins(season=season, league=league)

    ######      WINS        ######

    def alltime_wins(self, season = None, league = None):
        games = Game.objects.all()
        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team__pk=self.pk)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        games = games.filter(NON_PRESEASON & GAME_PLAYED).filter(home_wins | away_wins)

        return games.count()

    def regular_season_wins(self, season = None, league = None):
        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(GAME_PLAYED & Q(game_type=GAME_TYPE_REGULARSEASON)).filter(home_wins | away_wins)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def presason_wins(self, season = None, league = None):
        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(GAME_PLAYED & Q(game_type=GAME_TYPE_PRESEASON)).filter(home_wins | away_wins)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def playoff_wins(self, season = None, league = None):
        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(GAME_PLAYED).filter(CHAMPIONSHIP_GAME | PLAYOFF_GAME).filter(home_wins | away_wins)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def championship_wins(self, season = None, league = None):
        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(GAME_PLAYED & CHAMPIONSHIP_GAME).filter(home_wins | away_wins)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()


    ######      losses        ######

    def losses(self, season = None, preseason = False, league = None):
        if preseason:
            return self.preseason_wins(season = season, league=None)

        return self.alltime_losses(season=season, league=league)

    def alltime_losses(self, season = None, league = None):
        home_team = Q(home_team__team=self)
        away_team = Q(away_team__team=self)

        away_losses = Q(home_score__lt=F('away_score')) & home_team
        home_losses = Q(away_score__lt=F('home_score')) & away_team
        games = Game.objects.filter(NON_PRESEASON & GAME_PLAYED).filter(home_losses | away_losses)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def preseason_losses(self, season = None, league = None):
        away_losses = Q(home_score__lt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_losses = Q(away_score__lt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(GAME_PLAYED & Q(game_type=GAME_TYPE_PRESEASON)).filter(home_losses | away_losses)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def regular_season_losses(self, season = None, league = None):
        away_losses = Q(home_score__lt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_losses = Q(away_score__lt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(GAME_PLAYED & Q(game_type=GAME_TYPE_REGULARSEASON)).filter(home_losses | away_losses)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def playoff_losses(self, season = None, league = None):
        away_losses = Q(home_score__lt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_losses = Q(away_score__lt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(CHAMPIONSHIP_GAME | PLAYOFF_GAME).filter(home_losses | away_losses)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def championship_losses(self, season = None, league = None):
        away_losses = Q(home_score__lt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_losses = Q(away_score__lt=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(CHAMPIONSHIP_GAME).filter(home_losses | away_losses)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    ######      ties        ######

    def ties(self, season = None, preseason = False, league = None):
        if preseason:
            return self.preseason_ties(season = season, league=None)

        return self.alltime_ties(season=season, league=league)

    def alltime_ties(self, season=None, league = None):

        away_ties = Q(home_score=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_ties = Q(away_score=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(GAME_PLAYED & NON_PRESEASON).filter(home_ties | away_ties)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def preseason_ties(self, season = None, league = None):
        away_ties = Q(home_score=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_ties = Q(away_score=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(PRESEASON_GAME).filter(home_ties | away_ties)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def regular_season_ties(self, season = None, league = None):
        away_ties = Q(home_score=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_ties = Q(away_score=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(REGULAR_GAME).filter(home_ties | away_ties)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def playoff_ties(self, season = None, league = None):
        away_ties = Q(home_score=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_ties = Q(away_score=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(CHAMPIONSHIP_GAME | ply_game).filter(home_ties | away_ties)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def championship_ties(self, season = None, league = None):
        away_ties = Q(home_score=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_ties = Q(away_score=F('home_score')) & Q(away_team__team__pk=self.pk)
        games = Game.objects.filter(CHAMPIONSHIP_GAME).filter(home_ties | away_ties)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        return games.count()

    def standing_points(self, season = None, league = None):
        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team__pk=self.pk)

        away_losses = Q(home_score__lt=F('away_score')) & Q(home_team__team__pk=self.pk)
        home_losses = Q(away_score__lt=F('home_score')) & Q(away_team__team__pk=self.pk)

        for_season = Q(home_team__season=season) & Q(away_team__season=season)
        games = Game.objects.filter(GAME_PLAYED)

        if season is not None:
            games = _games_for_season(games, season)
        if league is not None:
            games = _games_for_league(games, league)

        result = games.aggregate(
            points = Sum(
                Case(
                  When(home_wins | away_wins , then=Value(2) )
                , When(home_losses | away_losses, then=Value(-1) )
                , default=Value(0), output_field=IntegerField()
                ),
                output_field = IntegerField()
            ),
            wins = Sum(
                Case(
                  When(home_wins | away_wins, then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),

            games = Sum(
                Case(
                  When(Q(away_team__team=self) | Q(home_team__team=self), then=Value(1))
                , default=Value(0), output_field=IntegerField()
                )
            ),

        )

        points = result['points'] or 0
        wins = result['wins'] or 0
        games = result['games'] or 0

        if points <= 0:
            if wins > 0:
                return wins * 0.5
            else:
                return 0
        return points + games

    def league_titles(self):
        away_wins = Q(home_score__gt=F('away_score')) & Q(home_team__team=self)
        home_wins = Q(away_score__gt=F('home_score')) & Q(away_team__team=self)
        games = Game.objects.filter(GAME_PLAYED, game_type=GAME_TYPE_CHAMPIONSHIP)
        return games.filter( away_wins | home_wins ).count()


    def conference_titles(self):
        '''
        counts the number championship appearances
        '''
        home_chp_games = GAME_TYPE_CHAMPIONSHIP & Q(home_team__team=self) & ~Q(away_team__team=self)
        away_chp_games = GAME_TYPE_CHAMPIONSHIP & Q(away_team__team=self) & ~Q(home_team__team=self)
        return LeagueGame.objects.filter(home_chp_games | away_chp_games).count()

    def rushing_statistics(self, league = None, season = None):
        stats = RushingStatistic.objects.filter(player__team__team=self)

        if season is not None:
            stats = _stats_for_season(stats, season)
        if league is not None:
            stats = _stats_for_league(stats, league)

        result = stats.aggregate(
          carries = Coalesce(Sum('carries'), Value(0))
        , yards = Coalesce(Sum('yards'), Value(0))
        , fumbles = Coalesce(Sum('fumbles'), Value(0))
        , yac = Coalesce(Sum('yac'), Value(0))
        , touchdowns = Coalesce(Sum('touchdowns'), Value(0))
        )

        return result
