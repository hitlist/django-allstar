from allstar.core.views.api import PagedListEndpoint, DetailEndpoint, ActionEndpoint, HttpError, serialize
from ..models import League, Team, Season

class LeagueListResource(PagedListEndpoint):
    model = League

class LeagueMemberResource(PagedListEndpoint):
    model = Team
    methods = ['GET']

    def get_query_set(self, request, *args, **kwargs):
        league_pk = kwargs.get('pk')
        qs = Team.objects.filter(leagueteam__league=league_pk).select_related()
        return qs

    def serialize(self, obj):
        out = serialize(obj, include=[
            ('logo', lambda obj: obj.logo.medium.url if obj.logo else None)
        ])
        return out
