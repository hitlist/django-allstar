import re
import uuid
from django.utils.http import urlquote
from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_backends
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from allstar.core.models import ModelBase
from .managers import UserManager, BaseUserManager
from django.core.mail import send_mail
from django.contrib.auth.hashers import (
    check_password, make_password, is_password_usable
)

STANDINGS = (
    (1,"Good"),
    (2,"Retired"),
    (3,"Banned"),
)

##
#
#
##
class Allstar( ModelBase, AbstractUser):
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    id               = models.UUIDField('id',db_column='allstar_id', primary_key=True, db_index=True, editable=False, default=uuid.uuid4)
    avatar           = models.ForeignKey('darkroom.Photo',on_delete=models.CASCADE, null=True, blank=True, default=None)
    biography        = models.TextField( _(u"Biograpy"),  help_text=_(u"Tell the world about yourself") , blank=True )
    standing         = models.IntegerField( verbose_name=_("Standing"), choices=STANDINGS, default=1 )
    birthday         = models.DateField(  verbose_name=(u"Birthday"), blank=True, null=True, help_text='YYYY-MM-DD' )
    hs_attended      = models.CharField( verbose_name=_(u"High School Attended"), max_length=100, blank=True, null=True )
    college_attened  = models.CharField( _(u"College Attended"),max_length=100, blank=True, null=True )
    height_feet      = models.PositiveSmallIntegerField( help_text="Height in feet", blank=True, null=True, default=6 )
    height_inches    = models.PositiveSmallIntegerField( help_text="remaining height in inches", blank=True, null=True, default=1 )

    gallery          = models.ForeignKey("darkroom.Gallery", related_name="user_gallery", blank=True, null=True
                                , db_column="user_gallery", unique=True, on_delete=models.CASCADE
                                , help_text= "Each player can have one photo gallery specifically for them" )

    admin_objects = BaseUserManager()
    objects       = UserManager()


    class Meta:
        verbose_name = _(u"Allstar")
        verbose_name_plural = _(u"Allstars")
        app_label = 'authentication'
        db_table = "allstar_auth_user"
        abstract=False
        permissions = (
            ("view_user", "Can View Allstars"),
        )

