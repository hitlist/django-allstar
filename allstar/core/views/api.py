from django import forms
from restless.models import serialize
from restless.views import Endpoint
from restless.modelviews import (ListEndpoint, DetailEndpoint, ActionEndpoint)
from restless.http import (Http200, Http201, Http400, Http401, Http403,
                           Http404, Http409, Http500, JSONErrorResponse,
                           HttpError)


def get_offset(params):

    try:
        offset = params.pop('offset')
        return int(offset)
    except ValueError:
        return None
    except KeyError:
        return 0


def get_limit(params):
    try:
        limit = params.pop('limit')
        return int(limit)
    except ValueError:
        return None
    except KeyError:
        return 25


class PagedListEndpoint(ListEndpoint):
    methods = ['GET']
    per_page = 25

    def get(self, request, *args, **kwargs):
        offset = get_offset(request.params)
        limit = get_limit(request.params) or self.per_page
        if limit is None:
            return Http400('Invalid Limit parameter')
        if offset is None:
            return Http400('Invalid offset parameter')

        objects = self.get_query_set(request, *args, **kwargs)
        count = objects.count()

        return {
            'data': self.serialize(objects[offset:offset + limit]),
            'meta': {
                'total': count,
                'limit': limit,
                'offset': offset
            }
        }
