from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from django.forms import ValidationError
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User as DjangoUser
from django.utils.translation import gettext, gettext_lazy as _
from .models import Allstar

USERMODEL = get_user_model()

class AllStarCreationForm( forms.ModelForm):
    class Meta:
        model = Allstar
        fields = ('username', 'password', 'email', 'first_name', 'last_name',)
    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            USERMODEL._default_manager.get(username=username)
        except USERMODEL.DoesNotExist:
            return username
        raise ValidationError(self.error_messages['duplicate_username'])

class AllStarChangeForm(UserChangeForm):
    biography = forms.CharField(widget=admin.widgets.AdminTextareaWidget, required=False)
    birthday = forms.DateField(widget=admin.widgets.AdminDateWidget)
    college_attended = forms.CharField(widget=admin.widgets.AdminTextInputWidget, required=False)
    weight = forms.IntegerField(widget=admin.widgets.AdminIntegerFieldWidget)

    class Meta:
        model = USERMODEL
        fields = '__all__'

class AllStarAdmin( UserAdmin ):
    list_display= ("username", "email", "first_name", "last_name", "date_joined", "is_staff", "is_superuser")
    date_hierarchy = "date_joined"
    add_form = AllStarCreationForm
    form = AllStarChangeForm

    fieldsets = (
        ( None
        , {
            'fields': ('username', 'password')
          }
        )
       , ( _('Personal info')
         , {
             'fields': (
               'first_name'
             , 'last_name'
             , 'email'
             , 'avatar'
             , 'college_attended'
             , 'biography'
             , 'weight'
             )
           , 'classes': ('wide',)
           }
         )
       , ( _('Permissions')
         , {
             'fields': (
               'is_active'
             , 'is_staff'
             , 'is_superuser'
             , 'groups'
             , 'user_permissions'
           )}
        )
      , ( _('Important dates')
        , {
            'fields': (
              'last_login'
            , 'date_joined'
            , 'birthday'
            )
          }
        ),
    )

admin.site.register(Allstar, AllStarAdmin)
