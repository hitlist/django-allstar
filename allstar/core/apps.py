from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'allstar_core'
