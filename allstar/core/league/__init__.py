def get_qb_rating(statsDict=None):
    '''
        C = Passing Completions
        Y = Passing Yards Per Attempt
        T = Passing Touchdowns Per Attempt
        I + Interceptions Per Attempts
        Pass in a dictionary named statsDict if the Passing Statisitcs you wish to calculate
            statsDict['completions']
            statsDict['attempts']
            statsDict['yards']
            statsDict['touchdowns']
            statsDict['interceptions']
        If no dictionary is passed, the function will calculate a QB rating for current game the stats associated with.
    C = ( (self.passing_completions / self.passing_attempts)*100 - 30) / 20
    Y = ((self.passing_tot_yards / self.passing_attempts)-3) * (1/4)
    T = (self.passing_tds / self.passing_attempts) * 20
    I = 2.375 - ( (self.passing_interceptions / self.passing_attempts) * 25 )

    rating = (( max(min(C, 2.375),0 ) + max(min(Y,2.375),0)  + max(min(T, 2.375),0) + max(min(I,2.375),0))/ 6) *100
    '''
    if statsDict is not None:
        try:
            C = float(( (float(statsDict['completions']) / float(statsDict['attempts'])*100 - 30)) / 20 )
            Y = ((statsDict['yards'] / statsDict['attempts']) - 3) /4
            T = float((statsDict['touchdowns']) / float(statsDict['attempts']) * 20)
            I = float(2.375 - ((float(statsDict['interceptions']) / float(statsDict['attempts'])) * 25 ))

            rating = (( max(min(C, 2.375),0 ) + max(min(Y,2.375),0)  + max(min(T, 2.375),0) + max(min(I,2.375),0))/ 6) *100
            return round(rating,1)
        except:
            return 0
    return 0
