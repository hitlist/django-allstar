import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='allstar',
    version='0.1',
    packages=find_packages(exclude=['__pycache__', '*.__pycache__', '__pycache.*', '*.__pycache__.*']),
    include_package_data=True,
    license='MIT License',  # example license
    description='DJango framework for building sport management applications',
    long_description=README,
    author='Eric Satterwhite',
    author_email='eric@codedependant.net',
    install_requires = [
        'sqlparse>=0.2.3',
        'DjangoRestless=0.0.10'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.11',  # replace "X.Y" as appropriate
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',  # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # Replace these appropriately if you are stuck on Python 2.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
