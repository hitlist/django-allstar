from django.contrib.contenttypes.models import ContentType

def get_generic_object(request, ct_id, obj_id):
    type = ContentType.objects.get(pk = ct_id)
    model = type.model_class()
    obj = model.objects.get(pk = obj_id)
    return obj
