from allstar.core.views.api import PagedListEndpoint, DetailEndpoint, Http404, JSONErrorResponse, HttpError
from ..models import Position

class PositionResource(PagedListEndpoint):
    model = Position

    def get_query_set(self, request, *args, **kwargs):
        return self.model.objects.all()

class PositionDetailResource(DetailEndpoint):
    model = Position

    def get_instance(self, request, *args, **kwargs):
        print("DETAIL")
        try:
            return self.model.objects.get(id=kwargs.get('pk'))
        except Position.DoesNotExist:
            raise HttpError(404, 'not found')

