import datetime
from django.utils.text import slugify
from django.contrib.auth import get_user_model
from django.test import TestCase
from ...models import Team, League, Season, Conference, LeagueTeam, Stadium, Game
from ...constants import GAME_TYPE_REGULARSEASON, GAME_TYPE_PLAYOFF, GAME_TYPE_CHAMPIONSHIP
User = get_user_model()

class TeamRecordTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.founder_one = User.objects.create(username='foobar', email='foo@bar.com')
        cls.founder_two = User.objects.create(username='barfoo', email='bar@foo.com')

        cls.ifl = League.objects.create(
            name='Ironman Football League',
            abbreviation='IFL',
            founded=datetime.date.today()
        )
        cls.nafl = League.objects.create(
            name='North American Football League',
            abbreviation='NAFL',
            founded=datetime.date.today()
        )

        cls.ifl.founders.add(cls.founder_one)
        cls.ifl.founders.add(cls.founder_two)

        conferences = Conference.objects.bulk_create([
            Conference(name='National', slug='national'),
            Conference(name='American', slug='american'),
            Conference(name='Eastern', slug='eastern'),
            Conference(name='Western', slug='wester')
        ])
        cls.national_conference = conferences[0]
        cls.american_conference = conferences[1]
        cls.eastern_conference = conferences[2]
        cls.western_conference = conferences[3]

        cls.season_2017 = Season.objects.create(year=2017)
        cls.season_2016 = Season.objects.create(year=2016)


        # Teams
        teams =Team.objects.bulk_create([
            Team(name='Hitmen', year_established='2010-01-01', nickname='MSK', slug='hitmen', city='muskego' ),
            Team(name='Raiders', year_established='2010-01-01', nickname='RDS', slug='raiders', city='milwaukee' ),
            Team(name='Mustangs', year_established='2010-01-01', nickname='MST', slug='mustangs', city='Madison' ),
            Team(name='Maniacs', year_established='2010-01-01', nickname='MAN', slug='maniacs', city='milwaukee' ),
            Team(name='Spartans', year_established='2010-01-01', nickname='SPT', slug='spartans', city='Wauwatosa' ),
            Team(name='Venom', year_established='2010-01-01', nickname='VNM', slug='venom', city='milwaukee' ),
            Team(name='Sting', year_established='2010-01-01', nickname='VNM', slug='sting', city='anitoch' ),
        ])

        stadiums = Stadium.objects.bulk_create([
            Stadium(
                name="Stadium 1", slug='stadium-1', address='123 main st', state='WI',
                zip_code='12345', field_type=1, lights=False, consessions=False
            ),
            Stadium(
                name="Stadium 2", slug='stadium-2', address='432 main st', state='WI',
                zip_code='12345', field_type=1, lights=False, consessions=False
            )
        ])


        cls.hitmen   = teams[0]
        cls.raiders  = teams[1]
        cls.mustangs = teams[2]
        cls.maniacs  = teams[3]
        cls.spartans = teams[4]
        cls.venom    = teams[5]
        cls.sting    = teams[6]


        # League - Team Memberships
        members = LeagueTeam.objects.bulk_create([
            LeagueTeam(team=cls.hitmen, season=cls.season_2017, league=cls.ifl, conference=cls.national_conference, stadium=stadiums[0]),
            LeagueTeam(team=cls.raiders, season=cls.season_2017, league=cls.ifl, conference=cls.national_conference, stadium=stadiums[0]),
            LeagueTeam(team=cls.mustangs, season=cls.season_2017, league=cls.ifl, conference=cls.american_conference, stadium=stadiums[0]),

            LeagueTeam(team=cls.maniacs, season=cls.season_2017, league=cls.nafl, conference=cls.eastern_conference, stadium=stadiums[1]),
            LeagueTeam(team=cls.spartans, season=cls.season_2017, league=cls.nafl, conference=cls.eastern_conference, stadium=stadiums[1]),
            LeagueTeam(team=cls.venom, season=cls.season_2017, league=cls.nafl, conference=cls.western_conference, stadium=stadiums[1]),

            LeagueTeam(team=cls.hitmen, season=cls.season_2016, league=cls.nafl, conference=cls.western_conference, stadium=stadiums[0]),
            LeagueTeam(team=cls.raiders, season=cls.season_2016, league=cls.nafl, conference=cls.eastern_conference, stadium=stadiums[0]),
            LeagueTeam(team=cls.mustangs, season=cls.season_2016, league=cls.nafl, conference=cls.western_conference, stadium=stadiums[0]),
            LeagueTeam(team=cls.sting, season=cls.season_2016, league=cls.nafl, conference=cls.western_conference, stadium=stadiums[0]),
            LeagueTeam(team=cls.venom, season=cls.season_2016, league=cls.nafl, conference=cls.eastern_conference, stadium=stadiums[1]),
        ])

        cls.hitmen_2017   = members[0]
        cls.raiders_2017  = members[1]
        cls.mustangs_2017 = members[2]
        cls.maniacs_2017  = members[3]
        cls.spartans_2017 = members[4]
        cls.venom_2017    = members[5]

        cls.hitmen_2016   = members[6]
        cls.raiders_2016  = members[7]
        cls.mustangs_2016 = members[8]
        cls.sting_2016    = members[9]
        cls.venom_2016    = members[10]

        now = datetime.datetime.now()
        Game.objects.bulk_create([
            # win
            Game(home_team=cls.hitmen_2017, home_score=33, away_team=cls.mustangs_2017, away_score=11, date_played=now, status=2),
            # loss
            Game(home_team=cls.hitmen_2017, home_score=14, away_team=cls.spartans_2017, away_score=22, date_played=now, status=2),
            #win
            Game(home_team=cls.hitmen_2017, home_score=33, away_team=cls.maniacs_2017, away_score=11, date_played=now, status=2),
            #tie
            Game(home_team=cls.hitmen_2017, home_score=33, away_team=cls.raiders_2017, away_score=33, date_played=now, status=2),
            #loss
            Game(away_team=cls.hitmen_2017, away_score=33, home_team=cls.venom_2017, home_score=43, date_played=now, status=2),
            #loss
            Game(home_team=cls.hitmen_2016, home_score=12, away_team=cls.mustangs_2016, away_score=22, date_played=now, status=2),
            #win
            Game(home_team=cls.raiders_2016, home_score=33, away_team=cls.hitmen_2016, away_score=44, date_played=now, status=2),
            #win
            Game(home_team=cls.venom_2016, home_score=22, away_team=cls.hitmen_2016, away_score=28, date_played=now, status=2),
            #win
            Game(home_team=cls.mustangs_2016, home_score=32, away_team=cls.hitmen_2016, away_score=44, game_type=GAME_TYPE_CHAMPIONSHIP, date_played=now, status=2),
            #sceduled
            Game(home_team=cls.venom_2016, home_score=32, away_team=cls.raiders_2016, away_score=44, game_type=GAME_TYPE_CHAMPIONSHIP, date_played=now, status=1),
        ])

    def test_record(self):
        hitmen = self.hitmen
        venom = self.venom
        spartans = self.spartans
        mustangs = self.mustangs

        wins, losses, ties = hitmen.record()
        self.assertEqual(wins, 5)
        self.assertEqual(losses, 3)
        self.assertEqual(ties, 1)

        wins, losses, ties = hitmen.record(season=2017)
        self.assertEqual(wins, 2)
        self.assertEqual(losses, 2)
        self.assertEqual(ties, 1)

        wins, losses, ties = hitmen.record(season=2016)
        self.assertEqual(wins, 3)
        self.assertEqual(losses, 1)
        self.assertEqual(ties, 0)

        wins, losses, ties = hitmen.record(league=self.nafl)
        self.assertEqual(wins, 3)
        self.assertEqual(losses, 1)
        self.assertEqual(ties, 0)

    def test_wins_against(self):
        hitmen = self.hitmen
        venom = self.venom
        spartans = self.spartans
        mustangs = self.mustangs

        self.assertEqual(hitmen.wins_against(venom), 1)
        self.assertEqual(hitmen.wins_against(mustangs), 2)
        self.assertEqual(hitmen.wins_against(spartans), 0)
        self.assertEqual(venom.wins_against(hitmen), 1)
        self.assertEqual(hitmen.wins_against(mustangs, league=self.ifl), 1)
        self.assertEqual(hitmen.wins_against(mustangs, season=self.season_2016), 1)
        self.assertEqual(hitmen.wins_against(venom, season=self.season_2017), 0)
        self.assertEqual(hitmen.wins_against(venom, season=self.season_2016, league=self.nafl), 1)
        self.assertEqual(hitmen.wins_against(mustangs, season=self.season_2017, league=self.ifl), 1)
        self.assertEqual(hitmen.wins_against(self.maniacs, season=self.season_2016, league=self.nafl), 0)


    def test_wins(self):
        hitmen = self.hitmen
        self.assertEqual(hitmen.wins(), 5)
        self.assertEqual(hitmen.wins(league=self.ifl), 2)
        self.assertEqual(hitmen.wins(season=self.season_2016), 3)
        self.assertEqual(hitmen.wins(season=self.season_2016, league=self.nafl), 3)
        self.assertEqual(self.spartans.wins(), 1)

    def test_losses_against(self):
        spartans = self.spartans
        hitmen = self.hitmen
        maniacs = self.maniacs
        sting = self.sting
        mustangs = self.mustangs

        self.assertEqual(spartans.losses_against(hitmen), 0)
        self.assertEqual(maniacs.losses_against(sting), 0)
        self.assertEqual(mustangs.losses_against(hitmen), 2)
        self.assertEqual(mustangs.losses_against(hitmen, season=self.season_2017), 1)
        self.assertEqual(mustangs.losses_against(hitmen, season=self.season_2017, league=self.ifl), 1)
        self.assertEqual(mustangs.losses_against(hitmen, season=self.season_2016, league=self.nafl), 1)
        self.assertEqual(spartans.losses_against(hitmen, league=self.ifl), 0)
        self.assertEqual(mustangs.losses_against(hitmen, season=self.season_2016), 1)

    def test_losses(self):
        spartans = self.spartans
        hitmen = self.hitmen
        maniacs = self.maniacs
        sting = self.sting

        self.assertEqual(spartans.losses(), 0)
        self.assertEqual(maniacs.losses(), 1)
        self.assertEqual(sting.losses(), 0)
        self.assertEqual(sting.losses(season=self.season_2017), 0)
        self.assertEqual(sting.losses(season=self.season_2016), 0)
        self.assertEqual(sting.losses(season=self.season_2017, league=self.ifl), 0)

        self.assertEqual(hitmen.losses(), 3)
        self.assertEqual(hitmen.losses(season=self.season_2017), 2)
        self.assertEqual(hitmen.losses(league=self.nafl), 1)
        self.assertEqual(hitmen.losses(league=self.ifl), 2)
        self.assertEqual(hitmen.losses(season=self.season_2017, league=self.ifl), 2)
        self.assertEqual(hitmen.losses(season=self.season_2016, league=self.nafl), 1)
        self.assertEqual(hitmen.losses(season=self.season_2017, league=self.nafl), 0)

    def test_ties_against(self):
        hitmen = self.hitmen
        raiders = self.raiders

        self.assertEqual(hitmen.ties_against(raiders), 1)
        self.assertEqual(hitmen.ties_against(raiders, league=self.ifl), 1)
        self.assertEqual(hitmen.ties_against(raiders, league=self.ifl, season=self.season_2016), 0)
        self.assertEqual(hitmen.ties_against(self.mustangs), 0)
        self.assertEqual(hitmen.ties_against(raiders, league=self.nafl), 0)

    def test_ties(self):
        spartans = self.spartans
        hitmen = self.hitmen
        maniacs = self.maniacs
        sting = self.sting
        venom = self.venom
        raiders = self.raiders

        self.assertEqual(spartans.ties(), 0)
        self.assertEqual(maniacs.ties(), 0)
        self.assertEqual(hitmen.ties(), 1)
        self.assertEqual(venom.ties(), 0)
        self.assertEqual(raiders.ties(), 1)

        self.assertEqual(hitmen.ties(league=self.ifl), 1)
        self.assertEqual(hitmen.ties(league=self.nafl), 0)
        self.assertEqual(hitmen.ties(league=self.ifl, season=self.season_2016), 0)
        self.assertEqual(hitmen.ties(league=self.ifl, season=self.season_2017), 1)
