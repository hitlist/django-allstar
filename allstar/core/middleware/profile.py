"""
    Endpoint profiling middleware
    :module: corvisacloud.profile
    :author: Eric Satterwhite
    :date:   2013-04-30
"""

import sys
import os
import re
import cProfile
import pstats
import tempfile
import io

from django.utils.deprecation import MiddlewareMixin
from django.conf import settings

words_re = re.compile( r'\s+' )

group_prefix_re = [
    re.compile( "^.*/django/[^/]+" ),
    re.compile( "^(.*)/[^/]+$" ), # extract module path
    re.compile( ".*" ),           # catch strange entries
]

##
# middleware class that allows for endpoint profiling
# @class ProfileMiddleware
# @exetnds object
##
class ProfileMiddleware(MiddlewareMixin):
    """
        Uses the hotshot profiler to generate a stats view of time spent in the request / response cycle
    """
    def process_request( self, request ):
        if( settings.DEBUG and request.user.is_superuser  ) and 'prof' in request.GET.keys():
            self.tmpfile = tempfile.mktemp()
            self.prof = cProfile.Profile()
            self.prof.enable()

    def process_view( self, request, callback, callback_args, callback_kwargs ):
        if (settings.DEBUG or request.user.is_superuser) and 'prof' in request.GET.keys():
            if getattr(self, 'prof', None) is None:
                self.tmpfile = tempfile.mktemp()
                self.prof = cProfile.Profile()
                self.prof.enable()
            return self.prof.runcall(callback, request, *callback_args, **callback_kwargs)

    def get_group( self, file ):
        for g in group_prefix_re:
            name = g.findall( file )
            if name:
                return name[0]

    def get_summary( self, results_dict, sum ):
        lst = [ (item[1], item[0]) for item in results_dict.items() ]
        lst.sort( reverse = True )
        lst = lst[:40]

        res = "      tottime\n"
        for item in lst:
            res += "%4.1f%% %7.3f %s\n" % ( 100*item[0]/sum if sum else 0, item[0], item[1] )

        return res

    def summary_for_files( self, stats ):
        """
            Produces overall summary of stats for results set
        """
        stats = stats.split("\n")[5:]

        mystats = {}
        mygroups = {}

        sum = 0

        for s in stats:
            fields = words_re.split(s);
            if len(fields) == 7:
                time = float(fields[2])
                sum += time
                file = fields[6].split(":")[0]

                if not file in mystats:
                    mystats[file] = 0
                mystats[file] += time

                group = self.get_group(file)
                if not group in mygroups:
                    mygroups[ group ] = 0
                mygroups[ group ] += time

        out = "<pre>\n\n" + \
               " ---- By file ----\n\n" + self.get_summary(mystats,sum) + "\n" + \
               " ---- By group ---\n\n" + self.get_summary(mygroups,sum) + \
               "</pre>"
        return bytes(out, 'utf8')

    def process_response( self, request, response ):
        if (settings.DEBUG and 'prof' in request.GET.keys()):
            self.prof.disable()
            out = io.StringIO()
            ps = pstats.Stats(self.prof, stream=out).sort_stats('cumtime', 'calls')
            ps.print_stats()

            stats_str = out.getvalue()

            if response and response.content and stats_str:
                response.content = "<pre>" + stats_str + "</pre>"

            response.content += self.summary_for_files(stats_str)


        return response
