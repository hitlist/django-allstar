from django.db.models import Manager
from allstar.core.managers import ArchiveManager
from django.contrib.auth.models import UserManager as DjangoUserManager

class BaseUserManager( DjangoUserManager):
	def get_by_natural_key(self, username):
	    return self.get(username=username)

class UserManager(ArchiveManager, BaseUserManager):
	pass
