from django.db.models import (Manager, Q)

class ArchiveManager(Manager):

    def get_query_set( self ):
        now = timezone.now()

        return super(ArchiveManager, self).get_query_set().filter(archived__isnull=False)
