from django.test import TestCase

# Create your tests here.

class CoreTest(TestCase):
    value = 1

    def setUp(self):
        print('setup')
        self.value = 2

    def test_value(self):
        self.assertEqual(self.value, 2, 'value=2')
