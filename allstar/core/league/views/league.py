from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from ..models.league import League, Season, Team

class LeagueList(ListView):
    model = League
    template_name = 'league/league_list.html'
    def get_context_data(self, **kwargs):
        context = super(LeagueList, self).get_context_data(**kwargs)
        context['object_list'] = self.model.objects.select_related().prefetch_related().order_by('name')
        context['seasons'] = Season.objects.all()
        pk = self.kwargs.get('pk')
        context['active_league'] = context['object_list'].get(pk=pk) if pk else context['object_list'].first()
        return context

class LeagueDetail(LeagueList):
    model = League
    template_name = 'league/league_list.html'

class LeagueTeamList(TemplateView):
    template_name = "league/partial_team_list.html"
    def get_context_data(self, **kwargs):
        context = super(LeagueTeamList, self).get_context_data(**kwargs)
        year = self.kwargs.get('year')
        league = self.kwargs.get('pk')
        teams = Team.objects.select_related().filter(
            leagueteam__league__pk=league,
            leagueteam__season__pk=year
        )
        context['teams'] = teams
        return context
