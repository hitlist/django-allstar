from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from ..models.league import League, Season, Team


class TeamView(DetailView):
    model = Team

    def get_context_data(self, **kwargs):
        context = super(TeamView, self).get_context_data(**kwargs)
        context['object'] = Team.objects.get(pk=self.kwargs.get('pk'))
        return context
