from django.contrib import admin
from imagekit.admin import AdminThumbnail
from .models import Photo, Gallery

class PhotoAdmin(admin.ModelAdmin):
    model = Photo
    list_display = ('title', 'admin_thumbnail',  )
    admin_thumbnail = AdminThumbnail(image_field = 'avatar')

admin.site.register(Photo, PhotoAdmin)
admin.site.register(Gallery)
